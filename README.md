# FastoCloud OTT Panel Frontend

[![Join the chat at https://discord.com/invite/cnUXsws](https://img.shields.io/discord/584773460585086977?label=discord)](https://discord.com/invite/cnUXsws)

### Description:
UI for FastoCloud OTT, backend you can find [here](https://gitlab.com/fastogt/fastocloud_ott/gobackend)

![English Stripe](https://gitlab.com/fastogt/fastocloud_ott/frontend/raw/main/docs/images/english_stripe.png) 
