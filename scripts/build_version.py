#!/usr/bin/env python3

import argparse
import os
import subprocess
import sys
import tarfile
from string import Template

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

CONFIG_TEMPLATE_IN_PATH = 'install/config.dart.in'
CONFIG_TEMPLATE_OUT_PATH = 'lib/config.dart'
BUNDLE_OUT_PATH = 'build/web'

PROJECT_NAME = 'branding'


def make_tarfile(output_filename, source_dir):
    with tarfile.open(output_filename, 'w:gz') as tar:
        tar.add(source_dir, arcname='fastocloud_iptv_build')


if __name__ == '__main__':
    host = 'https://fastocloud.com'
    name = 'last'

    parser = argparse.ArgumentParser(prog=PROJECT_NAME, usage='%(prog)s [options]')
    parser.add_argument('--host', help='build for host (default: {0})'.format(host), default=host)
    parser.add_argument('--name', help='name for archive (default: {0})'.format(name), default=name)
    argv = parser.parse_args()

    arg_host = argv.host
    arg_name = argv.name

    with open(CONFIG_TEMPLATE_IN_PATH) as config:
        src = Template(config.read())
        with open(CONFIG_TEMPLATE_OUT_PATH, 'w') as new_config:
            new_config.write(src.substitute(host=arg_host))
            print('Successfully created config, host: {0}'.format(arg_host))

    build_project_line = ['flutter', 'build', 'web']
    result = subprocess.call(build_project_line)
    print('Command: {0}, finished with return code: {1}'.format(build_project_line, result))

    tar_path = '{0}.tar.gz'.format(arg_name)
    make_tarfile(tar_path, BUNDLE_OUT_PATH)
    print('Archive ready path: {0}'.format(tar_path))
