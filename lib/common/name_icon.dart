import 'package:flutter/material.dart';

class NameAndIcon extends StatelessWidget {
  final Widget icon;
  final Widget name;

  const NameAndIcon({required this.name, required this.icon});

  @override
  Widget build(BuildContext context) {
    return Row(children: [icon, const SizedBox(width: 16), Expanded(child: name)]);
  }
}
