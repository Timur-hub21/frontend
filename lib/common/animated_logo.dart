import 'package:flutter/material.dart';
import 'package:frontend/common/net_assets.dart';
import 'package:frontend/routing/router_delegate.dart';

class AnimatedLogoButton extends StatefulWidget {
  final double size;
  final String path;

  const AnimatedLogoButton({required this.path, required this.size});

  @override
  State<AnimatedLogoButton> createState() => _AnimatedLogoButtonState();
}

class _AnimatedLogoButtonState extends State<AnimatedLogoButton> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      highlightColor: Colors.transparent,
      hoverColor: Colors.transparent,
      splashColor: Colors.transparent,
      onTap: () {
        RouterDelegateEx.of(context).toHome();
      },
      child: MyAnimatedLogo(
        child: NetAssetsIcon(
          widget.path,
          height: widget.size,
          width: widget.size,
        ),
      ),
    );
  }
}

class MyAnimatedLogo extends StatefulWidget {
  const MyAnimatedLogo({
    Key? key,
    required this.child,
  }) : super(key: key);

  final Widget child;

  @override
  State<MyAnimatedLogo> createState() => _MyAnimatedLogoState();
}

class _MyAnimatedLogoState extends State<MyAnimatedLogo> with TickerProviderStateMixin {
  late AnimationController controller;
  late Animation<double> animation;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 3),
    );
    animation = CurvedAnimation(
      parent: controller,
      curve: Curves.bounceOut,
    );
    controller.forward();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: SlideTransition(
        position: Tween<Offset>(
          begin: const Offset(0.0, -0.5),
          end: const Offset(0.0, 0.0),
        ).animate(animation),
        child: RotationTransition(
          turns: Tween(begin: 0.0, end: 1.0).animate(animation),
          child: SlideTransition(
            position: Tween<Offset>(
              begin: const Offset(0.5, 0.5),
              end: const Offset(0.0, 0.0),
            ).animate(animation),
            child: ScaleTransition(
              scale: animation,
              child: widget.child,
            ),
          ),
        ),
      ),
    );
  }
}
