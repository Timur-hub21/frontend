import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:frontend/fetcher.dart';
import 'package:frontend/service_locator.dart';
import 'package:http/http.dart';
import 'package:responsive_builder/responsive_builder.dart';

class LocalesPicker extends StatefulWidget {
  const LocalesPicker({this.onInit, this.onCountryChanged, this.onLanguageChanged, Key? key})
      : super(key: key);

  final void Function(String country, String language)? onInit;
  final void Function(String country)? onCountryChanged;
  final void Function(String language)? onLanguageChanged;

  @override
  State<LocalesPicker> createState() => _LocalesPickerState();
}

class _LocalesPickerState extends State<LocalesPicker> {
  String _currentCountry = '';
  String _currentLanguage = '';
  final List<List<String>> _countries = [];
  final List<List<String>> _languages = [];

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Response>(
        future: locator<Fetcher>().fetchGet('/locale'),
        builder: (context, snap) {
          if (snap.hasData) {
            final _response = snap.data!;
            if (_response.statusCode == 200) {
              final respData = httpDataResponseFromString(_response.body);
              final locale = respData!.contentMap()!;

              for (final List c in locale['countries']) {
                _countries.add(c.cast<String>());
              }
              for (final List l in locale['languages']) {
                _languages.add(l.cast<String>());
              }

              _currentCountry = locale['current_country'];
              _currentLanguage = locale['current_language'];
              widget.onInit?.call(_currentCountry, _currentLanguage);

              return ResponsiveBuilder(builder: (context, sizingInformation) {
                if (sizingInformation.isMobile || sizingInformation.localWidgetSize.width <= 360) {
                  return Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[_countryPicker(), _languagePicker()]);
                }
                return Row(children: <Widget>[
                  Expanded(child: _countryPicker()),
                  Expanded(child: _languagePicker())
                ]);
              });
            }
          }
          return const Center(child: CircularProgressIndicator());
        });
  }

  Widget _countryPicker() {
    return LocalePicker(
        current: _currentCountry,
        values: _countries,
        title: 'country'.tr(),
        onChanged: (c) {
          _currentCountry = c!;
          widget.onCountryChanged?.call(_currentCountry);
        });
  }

  Widget _languagePicker() {
    return LocalePicker(
        current: _currentLanguage,
        values: _languages,
        title: 'language'.tr(),
        onChanged: (c) {
          _currentLanguage = c!;
          widget.onLanguageChanged?.call(_currentLanguage);
        });
  }
}
