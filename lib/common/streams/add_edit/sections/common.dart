import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class OptionalFieldTile extends StatefulWidget {
  final bool init;
  final String title;
  final void Function(bool)? onChanged;
  final Widget Function() builder;

  const OptionalFieldTile(
      {required this.title, required this.init, this.onChanged, required this.builder});

  @override
  _OptionalFieldTileState createState() => _OptionalFieldTileState();
}

class _OptionalFieldTileState extends State<OptionalFieldTile> {
  late bool _value;

  @override
  void initState() {
    super.initState();
    _value = widget.init;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          //if (_value) const Divider(),
          CheckboxListTile(
              title: Text(widget.title),
              value: _value,
              onChanged: (value) {
                widget.onChanged?.call(value!);
                setState(() {
                  _value = value!;
                });
              }),
          if (_value) widget.builder(),
          //if (_value) const Divider()
        ]);
  }
}

class IOFieldBorder extends StatelessWidget {
  final Widget child;
  final VoidCallback? onDelete;

  const IOFieldBorder({required this.child, this.onDelete});

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Expanded(
          child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: Theme.of(context).brightness == Brightness.light
                              ? Colors.black38
                              : Colors.white38),
                      borderRadius: const BorderRadius.all(Radius.circular(8.0))),
                  child: child))),
      if (onDelete != null)
        IconButton(tooltip: 'remove'.tr(), icon: const Icon(Icons.close), onPressed: onDelete)
    ]);
  }
}

class IOTypeRadioTile extends StatelessWidget {
  final String title;
  final bool selected;
  final void Function() onChanged;

  const IOTypeRadioTile({required this.title, required this.selected, required this.onChanged});

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          onChanged();
        },
        child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(children: [
              Icon(selected ? Icons.radio_button_on : Icons.radio_button_off,
                  color: selected ? Theme.of(context).colorScheme.secondary : null),
              Text(title)
            ])));
  }
}
