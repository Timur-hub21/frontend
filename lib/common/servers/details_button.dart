import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';

class DetailsButton extends FlatButtonEx {
  DetailsButton([VoidCallback? onPressed])
      : super.filled(text: 'details'.tr(), onPressed: onPressed);
}
