import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:frontend/constants.dart';

class NetAssetsIcon extends StatelessWidget {
  final String link;
  final double? width;
  final double? height;

  const NetAssetsIcon(this.link, {this.width, this.height});

  String assetsLink() {
    return LOGO_PATH;
  }

  Widget defaultIcon() {
    return Image.asset(assetsLink(), height: height, width: width);
  }

  @override
  Widget build(BuildContext context) {
    return ClipRect(child: image());
  }

  Widget image() {
    return CachedNetworkImage(
        imageUrl: link,
        placeholder: (context, url) => defaultIcon(),
        errorWidget: (context, url, error) => defaultIcon(),
        width: width,
        height: height);
  }
}
