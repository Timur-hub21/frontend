import 'package:flutter/material.dart';
import 'package:frontend/common/net_assets.dart';
import 'package:frontend/routing/router_delegate.dart';

class LogoButton extends StatelessWidget {
  final double size;
  final String path;

  const LogoButton({required this.path, required this.size});

  @override
  Widget build(BuildContext context) {
    return InkWell(
        highlightColor: Colors.transparent,
        hoverColor: Colors.transparent,
        splashColor: Colors.transparent,
        onTap: () {
          RouterDelegateEx.of(context).toHome();
        },
        child: NetAssetsIcon(path, height: size, width: size));
  }
}
