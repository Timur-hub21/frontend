import 'package:flutter/material.dart';

class ScrollbarEx extends StatefulWidget {
  const ScrollbarEx({required this.builder, Key? key}) : super(key: key);

  final Widget Function(ScrollController controller) builder;

  static ScrollbarExState? of(BuildContext context) {
    return context.findAncestorStateOfType<ScrollbarExState>();
  }

  @override
  State<ScrollbarEx> createState() => ScrollbarExState();
}

class ScrollbarExState extends State<ScrollbarEx> {
  final ScrollController _controller = ScrollController();

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
        isAlwaysShown: true, controller: _controller, child: widget.builder(_controller));
  }

  void refresh() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _controller.position.didUpdateScrollPositionBy(0);
    });
  }
}
