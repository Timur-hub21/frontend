import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/cupertino.dart';

class BrandInfo with ChangeNotifier {
  OttSettings _brand;

  OttSettings get brand => _brand;

  BrandInfo(OttSettings brand) : _brand = brand;

  set brand(OttSettings brand) {
    _brand = brand;
    notifyListeners();
  }
}
