import 'package:flutter_common/flutter_common.dart';
import 'package:frontend/config.dart';
import 'package:frontend/models/widgets/settings.dart';
import 'package:http/http.dart';

class Fetcher extends IFetcher {
  static const _API_VERSION = '/panel_pro/api';

  static Fetcher? _instance;
  final String _frontendServerUrl = FRONTEND_SERVER_ENDPOINT;
  final String _backendServerUrl = BACKEND_SERVER_ENDPOINT;

  Fetcher._();

  Fetcher.getInstance() {
    _instance ??= Fetcher._();
  }

  @override
  Uri getBackendEndpoint(String path) {
    return _generateBackEndEndpoint(path);
  }

  Future<ServerInfo> getInfo() async {
    final response = await fetchGet('/info', [200]);
    final respData = httpDataResponseFromString(response.body);
    final data = respData!.contentMap()!;
    return ServerInfo.fromJson(data);
  }

  Future<Response> getPartners() {
    final response = get(Uri.parse('https://api.fastogt.com/api/partners'));
    return response;
  }

  Future<Response> getDownloads() {
    final response = get(Uri.parse('https://api.fastogt.com/api/downloads'));
    return response;
  }

  Future<Response> getSocials() {
    final response = get(Uri.parse('https://api.fastogt.com/api/socials'));
    return response;
  }

  // private:
  String _generateFrontEndEndpoint(String path) {
    return '$_frontendServerUrl$path';
  }

  Uri _generateBackEndEndpoint(String path) {
    final String base = '$_backendServerUrl$_API_VERSION';
    return Uri.parse('$base$path');
  }
}
