import 'package:flutter/material.dart';
import 'package:frontend/brand.dart';
import 'package:frontend/dashboard/dashboard_navigation.dart';
import 'package:frontend/dashboard/packages/details/server_details.dart';
import 'package:frontend/dashboard/packages/servers_page.dart';
import 'package:frontend/dashboard/profile/profile.dart';
import 'package:frontend/dashboard/providers/providers_page.dart';
import 'package:frontend/dashboard/settings/settings_page.dart';
import 'package:frontend/dashboard/subscribers/subscribers_page.dart';
import 'package:frontend/pages/developers/developers.dart';
import 'package:frontend/pages/download/downloads.dart';
import 'package:frontend/pages/header.dart';
import 'package:frontend/pages/home/home_page.dart';
import 'package:frontend/pages/home/privacy_policy.dart';
import 'package:frontend/pages/home/terms.dart';
import 'package:frontend/pages/login/sign_in.dart';
import 'package:frontend/pages/login/sign_up.dart';
import 'package:frontend/routing/page_not_found.dart';
import 'package:frontend/routing/route_config.dart';
import 'package:frontend/routing/routes.dart';
import 'package:provider/provider.dart';

class PageBuilder {
  final BuildContext context;

  PageBuilder(this.context);

  MaterialPage buildPage(RouteConfig state) {
    final Widget child = generateRoute(state);
    return MaterialPage(child: child);
  }

  Widget generateRoute(RouteConfig state) {
    final uri = state.uri;
    final String route = uri.path;
    final name = Provider.of<BrandInfo>(context).brand.title;
    switch (route) {
      // available without login
      case HOME_ROUTE:
        return _getHome(const HomePage(), true);
      case DOWNLOADS_ROUTE:
        return _getHome(const DownloadsPage(), true);
      case DEVELOPERS_ROUTE:
        return _getHome(const DevelopersPage(), true);
      case SIGN_IN_ROUTE:
        return const SignInPage(token: null);
      case SIGN_UP_ROUTE:
        return const SignUpPage();
      case CONFIRM_EMAIL:
        final String? token = uri.queryParameters['token'];
        return SignInPage(token: token);
      case TERMS_ROUTE:
        return TermsAndConditions(project: name);
      case PRIVACY_ROUTE:
        return PrivacyPolicy(project: name);
      // only for logged users
      case PROFILE_ROUTE:
        return _getDashboard(Profile(), route);
      case SUBSCRIBERS_ROUTE:
        return _getDashboard(const SubscribersWidget(), route);
      case PROVIDERS_ROUTE:
        return _getDashboard(const ProvidersWidget(), route);
      case SERVERS_ROUTE:
        return _handleServerRoute(state);
      case SETTINGS_ROUTE:
        return _getDashboard(SettingsPage(), route);
      default:
        return _getHome(const PageNotFound(), false);
    }
  }

  // private:
  Widget _getHome(Widget child, bool needFooter) {
    return DashboardHeader(child, needFooter);
  }

  Widget _getDashboard(Widget child, String route) {
    return _getHome(
        DashBoardNavigation(route, KeyedSubtree(key: GlobalKey(debugLabel: route), child: child)), false);
  }

  Widget _handleServerRoute(RouteConfig state) {
    final uri = state.uri;
    final String route = uri.path;
    final String? id = uri.queryParameters['id'];
    if (id != null) {
      return _getDashboard(ServerDetailsPage(id), route);
    }
    return _getDashboard(const ServersPage(), route);
  }
}
