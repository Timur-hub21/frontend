import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:frontend/routing/route_config.dart';
import 'package:frontend/routing/routes.dart';

class RouteParser extends RouteInformationParser<RouteConfig> {
  final Provider? profile;

  RouteParser(this.profile);

  /// Handles navigating from browser bar
  @override
  Future<RouteConfig> parseRouteInformation(RouteInformation routeInformation) async {
    final uri = Uri.parse(routeInformation.location!);
    final route = _makeConfig(uri);
    return SynchronousFuture<RouteConfig>(route);
  }

  /// Handles internal navigating
  @override
  RouteInformation restoreRouteInformation(RouteConfig config) {
    final route = _makeRouteInformation(config);
    return route;
  }

  RouteConfig _makeConfig(Uri uri) {
    //String route = uri.pathSegments[0];
    switch (uri.path) {
      // available without login
      case HOME_ROUTE:
        return const RouteConfig.home();
      case DOWNLOADS_ROUTE:
        return const RouteConfig.downloads();
      case DEVELOPERS_ROUTE:
        return const RouteConfig.developers();
      case SIGN_UP_ROUTE:
        return const RouteConfig.signUp();
      case SIGN_IN_ROUTE:
        return const RouteConfig.signIn();
      case CONFIRM_EMAIL:
        final String? token = uri.queryParameters['token'];
        if (token == null) {
          return const RouteConfig.signUp();
        }
        return RouteConfigParams.confirmEmail(token);
      case TERMS_ROUTE:
        return const RouteConfig.terms();
      default:
        final tmpConfig = _makeLoginConfig(uri);
        if (tmpConfig != null) {
          return _checkLogin(tmpConfig);
        }
        return RouteConfig(uri.toString());
    }
  }

  // only for logged users
  RouteConfig? _makeLoginConfig(Uri uri) {
    switch (uri.path) {
      case PROFILE_ROUTE:
        return const RouteConfig.profile();
      case SUBSCRIBERS_ROUTE:
        return const RouteConfig.subscribers();
      case PROVIDERS_ROUTE:
        return const RouteConfig.providers();
      case SERVERS_ROUTE:
        final String? id = uri.queryParameters['id'];
        if (id != null) {
          return DetailsRouteConfig.servers(id);
        }
        return const RouteConfig.servers();
      default:
        return null;
    }
  }

  RouteConfig _checkLogin(RouteConfig config) {
    return profile == null ? const RouteConfig.signIn() : config;
  }

  RouteInformation _makeRouteInformation(RouteConfig config) {
    return RouteInformation(location: config.path);
  }
}
