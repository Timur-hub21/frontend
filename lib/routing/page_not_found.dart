import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class PageNotFound extends StatelessWidget {
  static const double _size = 96;

  const PageNotFound();

  @override
  Widget build(BuildContext context) {
    final child = Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      const Icon(Icons.search, size: _size),
      Text('page-not-found'.tr(), style: const TextStyle(fontSize: _size / 2))
    ]);
    return Center(child: SingleChildScrollView(child: child));
  }
}
