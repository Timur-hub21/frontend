import 'package:frontend/routing/routes.dart';

class RouteConfig {
  final String path;

  const RouteConfig(this.path);

  const RouteConfig.home() : path = HOME_ROUTE;

  const RouteConfig.downloads() : path = DOWNLOADS_ROUTE;

  const RouteConfig.developers() : path = DEVELOPERS_ROUTE;

  const RouteConfig.signUp() : path = SIGN_UP_ROUTE;

  const RouteConfig.signIn() : path = SIGN_IN_ROUTE;

  const RouteConfig.terms() : path = TERMS_ROUTE;

  const RouteConfig.privacy() : path = PRIVACY_ROUTE;

  // only for logged users
  const RouteConfig.profile() : path = PROFILE_ROUTE;

  const RouteConfig.subscribers() : path = SUBSCRIBERS_ROUTE;

  const RouteConfig.providers() : path = PROVIDERS_ROUTE;

  const RouteConfig.servers() : path = SERVERS_ROUTE;

  const RouteConfig.settings() : path = SETTINGS_ROUTE;

  Uri get uri {
    return Uri.parse(path);
  }

  @override
  String toString() {
    return "RouteConfig{uriPath : $path}";
  }
}

class RouteConfigParams extends RouteConfig {
  RouteConfigParams(String path, Map<String, dynamic> params) : super(makePath(path, params));

  RouteConfigParams.confirmEmail(String token) : super(makePath(CONFIRM_EMAIL, {'token': token}));

  static String makePath(String path, Map<String, dynamic> params) {
    return Uri(path: path, queryParameters: params).toString();
  }
}

class DetailsRouteConfig extends RouteConfigParams {
  DetailsRouteConfig.servers(String id) : super(SERVERS_ROUTE, {'id': id});
}
