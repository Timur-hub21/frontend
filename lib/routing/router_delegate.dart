import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:frontend/routing/page_builder.dart';
import 'package:frontend/routing/route_config.dart';
import 'package:frontend/service_locator.dart';
import 'package:frontend/shared_prefs.dart';

class RouterDelegateEx extends RouterDelegate<RouteConfig>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<RouteConfig> {
  @override
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  RouteConfig? currentState;

  RouterDelegateEx();

  @override
  RouteConfig get currentConfiguration {
    return currentState ?? const RouteConfig.home();
  }

  static RouterDelegateEx of(BuildContext context) {
    return Router.of(context).routerDelegate as RouterDelegateEx;
  }

  @override
  Widget build(BuildContext context) {
    return Navigator(
        key: navigatorKey,
        pages: [PageBuilder(context).buildPage(currentConfiguration)],
        onPopPage: (route, result) {
          if (!route.didPop(result)) {
            return false;
          }
          return true;
        });
  }

  @override
  Future<void> setInitialRoutePath(RouteConfig configuration) {
    final settings = locator<LocalStorageService>();
    final bool checkValue = settings.check();
    if (checkValue) {
      return setNewRoutePath(const RouteConfig.signIn());
    }
    return setNewRoutePath(configuration);
  }

  /// Called after [parseRouteInformation], when navigating through browser
  @override
  Future<void> setNewRoutePath(RouteConfig newState) async {
    _changeState(newState);
    return SynchronousFuture<void>(null);
  }

  void toHome() {
    _changeState(const RouteConfig.home());
  }

  void toProfile() {
    _changeState(const RouteConfig.profile());
  }

  void _changeState(RouteConfig newState) {
    currentState = newState;
    notifyListeners();
  }
}
