const String HOME_ROUTE = '/';
const String DOWNLOADS_ROUTE = '/downloads';
const String DEVELOPERS_ROUTE = '/developers';
const String SIGN_IN_ROUTE = '/login';
const String SIGN_UP_ROUTE = '/join';
const String CONFIRM_EMAIL = '/confirm_email';
const String TERMS_ROUTE = '/terms';
const String PRIVACY_ROUTE = '/privacy';

const String PROFILE_ROUTE = '/profile';
const String SERVERS_ROUTE = '/servers';
const String SETTINGS_ROUTE = '/settings';
const String SUBSCRIBERS_ROUTE = '/subscribers';
const String PROVIDERS_ROUTE = '/providers';
