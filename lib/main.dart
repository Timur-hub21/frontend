import 'package:easy_localization/easy_localization.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:frontend/brand.dart';
import 'package:frontend/constants.dart';
import 'package:frontend/fetcher.dart';
import 'package:frontend/localization/app_localization.dart';
import 'package:frontend/profile.dart';
import 'package:frontend/routing/route_parser.dart';
import 'package:frontend/routing/router_delegate.dart';
import 'package:frontend/service_locator.dart';
import 'package:frontend/shared_prefs.dart';
import 'package:frontend/theme.dart';
import 'package:provider/provider.dart' as prov;
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  await setupLocator();
  runApp(FastoCloudPanel());
}

class FastoCloudPanel extends StatefulWidget {
  @override
  _FastoCloudPanel createState() {
    return _FastoCloudPanel();
  }
}

class _FastoCloudPanel extends State<FastoCloudPanel> {
  final delegate = RouterDelegateEx();

  late Locale savedLocale;

  // final Map<Locale, String> supportedLocales = {
  //   const Locale('en'): 'English',
  //   const Locale('ru'): 'Русский',
  // };

  @override
  void initState() {
    super.initState();
    _loadLocale();
    savedLocale = AppLocalization.supportedLocales.first;
  }

  @override
  Widget build(BuildContext context) {
    final settings = OttSettings(
      logo: NET_LOGO_URL,
      title: PROJECT,
      landing: LANDING_URL,
      theme: GlobalTheme.LIGHT,
    );
    final brand = BrandInfo(settings);
    return ChangeNotifierProvider<BrandInfo>(
      create: (context) {
        final fetcher = locator<Fetcher>();
        fetcher.getInfo().then((value) {
          brand.brand = value.brand;
        });
        return brand;
      },
      child: EasyLocalization(
        supportedLocales: AppLocalization.supportedLocales,
        path: 'install/assets/translations',
        startLocale: AppLocalization.supportedLocales.first,
        fallbackLocale: AppLocalization.supportedLocales.first,
        child: Builder(builder: (context) {
          final profileInfo = ProfileInfo(child: Builder(builder: (context) {
            final profile = ProfileInfo.of(context)!;
            return MaterialApp.router(
                title: brand.brand.title,
                debugShowCheckedModeBanner: false,
                localizationsDelegates: context.localizationDelegates,
                supportedLocales: context.supportedLocales,
                locale: context.locale,
                localeResolutionCallback: (locale, supportedLocales) {
                  for (final supportedLocale in supportedLocales) {
                    if (locale != null) {
                      if (supportedLocale.languageCode == locale.languageCode &&
                          supportedLocale.countryCode == locale.countryCode) {
                        return supportedLocale;
                      }
                    }
                  }
                  return supportedLocales.first;
                },
                theme: prov.Provider.of<BrandInfo>(context).brand.theme.toHumanReadable() ==
                        GlobalTheme.LIGHT_THEME
                    ? AppTheme.lightTheme
                    : AppTheme.darkTheme,
                routerDelegate: delegate,
                routeInformationParser: RouteParser(profile.profile));
          }));
          return profileInfo;
        }),
      ),
    );
  }

  void _loadLocale() {
    final settings = locator<LocalStorageService>();
    final langCode = settings.langCode();
    // final countryCode = settings.countryCode();
    if (langCode != null /*&& countryCode != null*/) {
      context.setLocale(Locale(langCode));
    }
  }
}
