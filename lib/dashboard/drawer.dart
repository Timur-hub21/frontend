import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:frontend/brand.dart';
import 'package:frontend/common/net_assets.dart';
import 'package:frontend/dashboard/dashboard_navigation.dart';
import 'package:frontend/models/destination.dart';
import 'package:frontend/profile.dart';
import 'package:frontend/routing/route_config.dart';
import 'package:frontend/routing/router_delegate.dart';
import 'package:frontend/service_locator.dart';
import 'package:provider/provider.dart';

class DashboardDrawer extends StatefulWidget {
  final String route;

  const DashboardDrawer(this.route);

  @override
  _DashboardDrawerState createState() {
    return _DashboardDrawerState();
  }
}

class _DashboardDrawerState extends State<DashboardDrawer> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DesktopDrawer((collapsed) {
      final bool isAdmin = ProfileInfo.of(context)!.profile?.isAdmin() ?? false;
      final List<Destination> destinations = DashBoardNavigation.destinations(isAdmin);
      final List<Widget> result = [_logo()];
      for (int i = 0; i < destinations.length; ++i) {
        result.add(_tile(destinations[i], collapsed));
      }
      result.add(_version(collapsed));
      return result;
    });
  }

  // private:
  Widget _logo() {
    final brand = Provider.of<BrandInfo>(context);
    final draw = DrawerHeader(child: NetAssetsIcon(brand.brand.logo, width: 320, height: 320));
    return Align(child: draw);
  }

  Widget _tile(Destination d, bool collapsed) {
    return _DrawerTile(
        selected: widget.route == d.route.path,
        icon: d.icon,
        title: d.title,
        collapsed: collapsed,
        onPressed: () {
          _navigate(d.route);
        });
  }

  Widget _version(bool collapsed) {
    final package = locator<PackageManager>();
    final String version = package.version();
    return _DrawerTile(icon: Icons.info, title: version, collapsed: collapsed);
  }

  void _navigate(RouteConfig route) {
    RouterDelegateEx.of(context).setNewRoutePath(route);
  }
}

class DesktopDrawer extends StatefulWidget {
  final List<Widget> Function(bool) tiles;

  const DesktopDrawer(this.tiles);

  @override
  _DesktopDrawerState createState() {
    return _DesktopDrawerState();
  }
}

class _DesktopDrawerState extends State<DesktopDrawer> {
  bool _collapsed = false;
  bool _collapsedTile = false;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
        width: _collapsed ? 56 : 210,
        duration: const Duration(milliseconds: 200),
        onEnd: () {
          if (!_collapsed) {
            setState(() {
              _collapsedTile = false;
            });
          }
        },
        child: Column(children: [
          Expanded(
              child: Align(
                  alignment: Alignment.topLeft,
                  child: SingleChildScrollView(
                      primary: false,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: widget.tiles(_collapsedTile))))),
          const Divider(thickness: 1, height: 0),
          _DrawerTile(
              icon: _collapsedTile ? Icons.arrow_forward_ios : Icons.arrow_back_ios,
              onPressed: () {
                setState(() {
                  _collapsed = !_collapsed;
                  if (_collapsed) {
                    _collapsedTile = true;
                  }
                });
              },
              collapsed: _collapsedTile,
              title: 'collapse-sidebar'.tr())
        ]));
  }
}

class _DrawerTile extends StatelessWidget {
  final IconData icon;
  final void Function()? onPressed;
  final String title;
  final bool selected;
  final bool collapsed;

  const _DrawerTile(
      {required this.icon,
      required this.title,
      this.onPressed,
      this.selected = false,
      this.collapsed = false});

  @override
  Widget build(BuildContext context) {
    if (!collapsed) {
      return _tile(context);
    }
    return Tooltip(message: title, child: _tile(context));
  }

  // private:
  Widget _tile(BuildContext context) {
    Color? color;
    if (selected) {
      color = Theme.of(context).primaryColor;
    }
    final child = InkWell(
        onTap: onPressed?.call,
        child: Row(children: [
          Padding(padding: const EdgeInsets.all(16), child: Icon(icon, color: color)),
          if (!collapsed)
            Text(title,
                style: Theme.of(context).textTheme.subtitle1!.copyWith(
                    color: color, fontWeight: selected ? FontWeight.bold : FontWeight.normal))
        ]));
    return SizedBox(height: 56, child: child);
  }
}
