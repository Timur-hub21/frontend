import 'package:easy_localization/easy_localization.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:frontend/brand.dart';
import 'package:frontend/common/net_assets.dart';
import 'package:frontend/common/streams/add_edit/sections/common.dart';
import 'package:frontend/dashboard/settings/settings_loader.dart';
import 'package:frontend/localization/app_localization.dart';
import 'package:frontend/models/widgets/settings.dart';
import 'package:frontend/service_locator.dart';
import 'package:frontend/shared_prefs.dart';
import 'package:provider/provider.dart' as provider;

class SettingsPage extends StatefulWidget {
  const SettingsPage();

  @override
  State<SettingsPage> createState() {
    return _SettingsPageState();
  }
}

class _SettingsPageState extends State<SettingsPage> {
  final SettingsLoader _loader = SettingsLoader();
  late PrivateSettings _settings;
  late Locale _selectedLocale;

  @override
  void initState() {
    super.initState();
    _loader.load();
  }

  @override
  void dispose() {
    _loader.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LoaderWidget<SettingsDataState>(
        loader: _loader,
        builder: (_, SettingsDataState state) {
          _settings = state.data;
          return Column(children: <Widget>[
            Row(children: [
              Expanded(child: NetAssetsIcon(_settings.logo)),
              Expanded(child: _logoField(), flex: 9)
            ]),
            _titleField(),
            _landingField(),
            _paymentField(state),
            Row(children: [
              Expanded(child: _theme()),
              Expanded(child: _locale()),
            ]),
            _onSave(),
          ]);
        });
  }

  Widget _titleField() {
    return TextFieldEx(
        hintText: 'name'.tr(),
        errorText: '${'enter'.tr()} ${'name'.tr()}',
        init: _settings.title,
        onFieldChanged: (val) => _settings.title = val);
  }

  Widget _logoField() {
    return TextFieldEx(
        hintText: '${'logo'.tr()} ${'url'.tr()}',
        errorText: '${'enter'.tr()} ${'url'.tr()} ${'logo'.tr()}',
        init: _settings.logo,
        onFieldChanged: (val) => _settings.logo = val);
  }

  Widget _landingField() {
    return TextFieldEx(
        hintText: '${'landing'.tr()} ${'url'.tr()}',
        errorText: '${'enter'.tr()} ${'landing'.tr()} ${'url'.tr()}',
        init: _settings.landing,
        onFieldChanged: (val) => _settings.landing = val);
  }

  Widget _paymentField(SettingsDataState state) {
    return OptionalFieldTile(
        onChanged: (val) {
          if (val) {
            _settings.payment = PaymentStripe.createDefault();
          } else {
            _settings.payment = null;
          }
        },
        title: 'payment-gateway-apis'.tr(),
        init: _settings.payment != null,
        builder: () {
          final currentPayment = _settings.payment!;
          return Column(children: [
            DropdownButtonEx<PaymentType>(
                onChanged: (value) {
                  if (value == PaymentType.PAYPAL) {
                    setState(() {
                      _settings.payment = PaymentPaypal.createDefault();
                    });
                  } else if (value == PaymentType.STRIPE) {
                    setState(() {
                      _settings.payment = PaymentStripe.createDefault();
                    });
                  }
                },
                value: currentPayment.type,
                values: PaymentType.values,
                itemBuilder: (value) {
                  return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value);
                }),
            if (currentPayment is PaymentPaypal) _paypalFields() else _stripeFields()
          ]);
        });
  }

  Widget _stripeFields() {
    final payment = _settings.payment! as PaymentStripe;
    return Column(children: [
      TextFieldEx(
          key: const Key('stripe_secret_key'),
          hintText: 'secret-key'.tr(),
          init: payment.stripe.secretKey,
          onFieldChanged: (val) {
            payment.stripe.secretKey = val;
          }),
      TextFieldEx(
          key: const Key('stripe_pub_key'),
          hintText: 'pub-key'.tr(),
          init: payment.stripe.pubKey,
          onFieldChanged: (val) {
            payment.stripe.pubKey = val;
          })
    ]);
  }

  Widget _paypalFields() {
    final payment = _settings.payment! as PaymentPaypal;
    return Column(children: [
      TextFieldEx(
        key: const Key('paypal_client_id'),
        hintText: 'Client ID',
        init: payment.paypal.clientId,
        onFieldChanged: (val) {
          payment.paypal.clientId = val;
        },
      ),
      TextFieldEx(
          key: const Key('paypal_client_secret'),
          hintText: 'Client Secret',
          init: payment.paypal.clientSecret,
          onFieldChanged: (val) {
            payment.paypal.clientSecret = val;
          }),
      DropdownButtonEx<PaypalMode>(
          value: payment.paypal.mode,
          values: PaypalMode.values,
          onChanged: (val) {
            payment.paypal.mode = val;
          },
          itemBuilder: (val) {
            return DropdownMenuItem(child: Text(val.toHumanReadable()), value: val);
          })
    ]);
  }

  Widget _theme() {
    return DropdownButtonEx<GlobalTheme>(
      itemBuilder: (value) {
        return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value);
      },
      values: GlobalTheme.values,
      value: _settings.theme,
      onChanged: (value) {
        _settings.theme = value;
      },
    );
  }

  Widget _locale() {
    _selectedLocale = context.locale;
    return DropdownButtonEx<Locale>(
      value: _selectedLocale,
      values: AppLocalization.supportedLocales,
      onChanged: (locale) {
        _selectedLocale = locale;
      },
      itemBuilder: (locale) {
        return DropdownMenuItem(child: Text(locale.languageCode.toUpperCase()), value: locale);
      },
    );
  }

  Widget _onSave() {
    final sharedPref = locator<LocalStorageService>();
    return ElevatedButton(
      onPressed: () {
        final settings = PrivateSettings(
          logo: _settings.logo,
          title: _settings.title,
          landing: _settings.landing,
          payment: _settings.payment,
          theme: _settings.theme,
        );
        final resp = _loader.saveSettings(settings);
        resp.then((value) {
          final brand = provider.Provider.of<BrandInfo>(context, listen: false);
          brand.brand = value;
          setState(() {
            _settings = value;
          });
        });
        // AppLocalizations.of(context)!.load(_selectedLocale);
        context.setLocale(_selectedLocale);
        sharedPref.setLangCode(_selectedLocale.languageCode);
      },
      child: Text('save'.tr()),
    );
  }
}
