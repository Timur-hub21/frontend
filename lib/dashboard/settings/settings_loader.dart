import 'package:flutter_common/http_response.dart';
import 'package:flutter_common/loader.dart';
import 'package:frontend/fetcher.dart';
import 'package:frontend/models/widgets/settings.dart';
import 'package:frontend/service_locator.dart';

class SettingsLoader extends ItemBloc {
  @override
  Future<SettingsDataState> loadDataRoute() {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchGet('/provider/settings');
    return response.then((value) {
      final respData = httpDataResponseFromString(value.body);
      final data = respData!.contentMap()!;
      final result = PrivateSettings.fromJson(data['settings']);
      return SettingsDataState(result);
    }, onError: (error) {
      throw error;
    });
  }

  Future<PrivateSettings> saveSettings(PrivateSettings settings) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchPost('/provider/settings', settings.toJson(), [200]);

    return response.then((value) {
      final respData = httpDataResponseFromString(value.body);
      final data = respData!.contentMap()!;
      final result = PrivateSettings.fromJson(data);
      return result;
    }, onError: (error) {
      throw error;
    });
  }
}

class SettingsDataState extends ItemDataState<PrivateSettings> {
  SettingsDataState(PrivateSettings data) : super(data);
}
