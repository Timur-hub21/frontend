import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:frontend/dashboard/drawer.dart';
import 'package:frontend/models/destination.dart';
import 'package:frontend/routing/route_config.dart';
import 'package:responsive_builder/responsive_builder.dart';

class DashBoardNavigation extends StatelessWidget {
  final String route;
  final Widget child;

  static List<Destination> destinations(bool isAdmin) {
    return isAdmin
        ?  <Destination>[
            Destination(const RouteConfig.profile(), 'profile'.tr(), Icons.dashboard),
            Destination(const RouteConfig.subscribers(), 'subscribers'.tr(), Icons.people_outline),
            Destination(const RouteConfig.providers(), 'providers'.tr(), Icons.accessibility),
            Destination(const RouteConfig.servers(), 'packages'.tr(), Icons.wifi_tethering),
            Destination(const RouteConfig.settings(), 'settings'.tr(), Icons.settings)
          ]
        :  <Destination>[
            Destination(const RouteConfig.profile(), 'profile'.tr(), Icons.dashboard),
            Destination(const RouteConfig.subscribers(), 'subscribers'.tr(), Icons.people_outline)
          ];
  }

  const DashBoardNavigation(this.route, this.child);

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizingInformation) {
      if (sizingInformation.isDesktop) {
        return Row(children: <Widget>[
          DashboardDrawer(route),
          const VerticalDivider(thickness: 1, width: 0),
          Expanded(child: child)
        ]);
      }
      return child;
    });
  }
}
