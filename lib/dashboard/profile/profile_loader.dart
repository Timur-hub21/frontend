import 'dart:async';

import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter_common/http_response.dart';
import 'package:flutter_common/loader.dart';
import 'package:frontend/fetcher.dart';
import 'package:frontend/service_locator.dart';

class ProviderLoader extends ItemBloc {
  @override
  Future<ProviderDataState> loadDataRoute() {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchGet('/provider/profile');
    return response.then((value) {
      final respData = httpDataResponseFromString(value.body);
      final data = respData!.contentMap()!;
      final provider = Provider.fromJson(data['user']);
      return ProviderDataState(provider);
    }, onError: (error) {
      throw error;
    });
  }
}

class ProviderDataState extends ItemDataState<Provider> {
  ProviderDataState(Provider data) : super(data);
}
