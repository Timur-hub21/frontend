import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:frontend/common/add_edit_dialog.dart';
import 'package:frontend/dashboard/subscribers/devices_loader.dart';

class DeviceDialog extends StatelessWidget {
  final Device init;
  final DevicesLoader loader;

  DeviceDialog.add(this.loader) : init = Device.createDefault();

  DeviceDialog.edit(this.loader, Device device) : init = device.copy();

  bool get isAdd => init.id == null;

  @override
  Widget build(BuildContext context) {
    return AddEditDialog(
        add: isAdd,
        maxWidth: AddEditDialog.NARROW_WIDTH,
        maxHeight: 120,
        children: <Widget>[
          TextFieldEx(
              hintText: 'Name',
              errorText: 'Enter device name',
              minSymbols: Device.MIN_NAME_LENGTH,
              maxSymbols: Device.MAX_NAME_LENGTH,
              init: init.name,
              onFieldChanged: (String term) {
                init.name = term;
              }),
          if (!isAdd) _StatusPicker(init)
        ],
        onSave: () {
          _save(context);
        });
  }

  void _save(BuildContext context) {
    if (!init.isValid()) {
      return;
    }
    final Future<Device> _response =
        isAdd ? loader.addSubscriberDevice(init) : loader.editSubscriberDevice(init);
    _response.then((Device device) {
      Navigator.of(context).pop(device);
    }, onError: (error) {
      showError(context, error);
    });
  }
}

class _StatusPicker extends DropdownButtonEx<DeviceStatus> {
  _StatusPicker(Device device)
      : super(
            hint: device.status.toHumanReadable(),
            value: device.status,
            values: DeviceStatus.values,
            onChanged: (c) {
              device.status = c;
            },
            itemBuilder: (DeviceStatus value) {
              return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value);
            });
}
