import 'package:easy_localization/easy_localization.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/errors.dart';
import 'package:flutter_common/loader.dart';
import 'package:flutter_common/widgets.dart';
import 'package:frontend/common/add_edit_dialog.dart';
import 'package:frontend/common/locale_picker.dart';
import 'package:frontend/common/scrollbar.dart';
import 'package:frontend/dashboard/packages/servers_loader.dart';
import 'package:frontend/dashboard/subscribers/subscribers_loader.dart';
import 'package:frontend/event_bus.dart';
import 'package:frontend/events/subscriber_events.dart';
import 'package:frontend/fetcher.dart';
import 'package:frontend/profile.dart';
import 'package:frontend/service_locator.dart';

class SubscriberDialog extends StatelessWidget {
  final Subscriber init;
  final SubscribersLoader loader;

  SubscriberDialog.add(this.loader) : init = Subscriber.createDefault();

  SubscriberDialog.edit(this.loader, Subscriber subscriber)
      : init = subscriber.copy()..password = null;

  bool get isAdd => init.id == null;

  @override
  Widget build(BuildContext context) {
    return AddEditDialog.narrow(
        add: isAdd,
        children: <Widget>[
          _Fields(init, isAdd),
          _ExpDateField(init),
          _StatusPicker(init),
          _LocalePickers(init),
          _ServersList(init.servers)
        ],
        onSave: () {
          _save(context);
        });
  }

  void _save(BuildContext context) {
    if (!init.isValid()) {
      return;
    }

    final Future<Subscriber> _response =
        isAdd ? loader.addSubscriber(init) : loader.editSubscriber(init);
    _response.then((Subscriber subscriber) {
      final eventBus = locator<FastoEventBus>();
      isAdd
          ? eventBus.publish(SubscriberAddEvent(subscriber))
          : eventBus.publish(SubscriberEditEvent(subscriber));
      Navigator.of(context).pop();
    }, onError: (error) {
      showError(context, error);
    });
  }
}

class _Fields extends StatelessWidget {
  const _Fields(this.sub, this.emptyPassword);

  final Subscriber sub;
  final bool emptyPassword;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          TextFieldEx(
              hintText: 'first-name'.tr(),
              errorText: '${'enter-your'.tr()} ${'first-name'.tr()}',
              maxSymbols: Subscriber.MAX_NAME_LENGTH,
              init: sub.firstName,
              onFieldChanged: (term) {
                sub.firstName = term;
              }),
          TextFieldEx(
              hintText: 'last-name'.tr(),
              errorText: '${'enter-your'.tr()} ${'last-name'.tr()}',
              maxSymbols: Subscriber.MAX_NAME_LENGTH,
              init: sub.lastName,
              onFieldChanged: (term) {
                sub.lastName = term;
              }),
          TextFieldEx(
              hintText: 'email'.tr(),
              errorText: '${'enter-your'.tr()} ${'email'.tr()}',
              init: sub.email,
              onFieldChanged: (term) {
                sub.email = term;
              },
              keyboardType: TextInputType.emailAddress),
          PassWordTextField(
              hintText: 'password'.tr(),
              errorText:
                  emptyPassword ? '${'enter'.tr()} ${'password'.tr()}' : null,
              onFieldChanged: (term) {
                sub.password = term;
              }),
          Row(children: [
            Expanded(
                child: ListTile(
                    contentPadding: const EdgeInsets.all(8),
                    title: Text('devices'.tr()),
                    subtitle: Text('max-device-count'.tr()))),
            SizedBox(
                width: 80,
                child: NumberTextField.integer(
                    minInt: 0,
                    canBeEmpty: false,
                    hintText: 'count'.tr(),
                    initInt: sub.maxDevicesCount,
                    onFieldChangedInt: (term) {
                      if (term != null) sub.maxDevicesCount = term;
                    }))
          ])
        ]));
  }
}

class _ExpDateField extends StatefulWidget {
  const _ExpDateField(this.sub);

  final Subscriber sub;

  @override
  _ExpDateFieldState createState() {
    return _ExpDateFieldState();
  }
}

class _ExpDateFieldState extends State<_ExpDateField> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
        title: Text('exp-date'.tr()),
        subtitle: Text(
            DateTime.fromMillisecondsSinceEpoch(widget.sub.expDate).toString()),
        onTap: () {
          final pick = showDatePicker(
              context: context,
              initialDate: DateTime.now(),
              firstDate: DateTime.now(),
              lastDate: DateTime(2100));
          pick.then((DateTime? date) {
            if (date != null) {
              setState(() {
                widget.sub.expDate = date.millisecondsSinceEpoch;
              });
            }
          });
        });
  }
}

class _StatusPicker extends DropdownButtonEx<SubscriberStatus> {
  _StatusPicker(Subscriber sub)
      : super(
            hint: sub.status.toHumanReadable(),
            value: sub.status,
            values: SubscriberStatus.values,
            onChanged: (c) {
              sub.status = c;
            },
            itemBuilder: (SubscriberStatus value) {
              return DropdownMenuItem(
                  child: Text(value.toHumanReadable()), value: value);
            });
}

class _LocalePickers extends StatelessWidget {
  const _LocalePickers(this.sub);

  final Subscriber sub;

  @override
  Widget build(BuildContext context) {
    return LocalesPicker(onInit: (country, language) {
      sub.country ??= country;
      sub.language ??= language;
      ScrollbarEx.of(context)!.refresh();
    }, onCountryChanged: (country) {
      sub.country = country;
    }, onLanguageChanged: (lang) {
      sub.language = lang;
    });
  }
}

class _ServersList extends StatefulWidget {
  final List<String> _selectedServers;

  const _ServersList(this._selectedServers);

  @override
  _ServersListState createState() {
    return _ServersListState();
  }
}

class _ServersListState extends State<_ServersList> {
  bool _selectedAll = false;
  late List<GoServer> _allServers = [];

  final ServersLoader _loader = ServersLoader();

  @override
  void initState() {
    super.initState();
    _loader.load();
  }

  @override
  void dispose() {
    _loader.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LoaderWidget<ServersDataState>(
        loader: _loader,
        onState: (_) => ScrollbarEx.of(context)!.refresh(),
        builder: (_, state) {
          _allServers = state.data;
          final size = widget._selectedServers.length;
          if (size > 0) {
            _selectedAll = size == _allServers.length;
          }

          return _list();
        });
  }

  Widget _header() {
    return ListTile(
        title: Text('${'servers'.tr()}/${'packages'.tr()}:',
            style: const TextStyle(fontWeight: FontWeight.bold)),
        onTap: _selectAll,
        trailing: Icon(Icons.select_all,
            color:
                _selectedAll ? Theme.of(context).colorScheme.secondary : null));
  }

  Widget _list() {
    final List<Widget> servers = [];
    _allServers.forEach((server) {
      final String sid = server.id!;
      final bool selected = widget._selectedServers.contains(sid);
      final checkbox = CheckboxListTile(
          contentPadding: const EdgeInsets.only(left: 16, right: 12),
          title: Text(server.name),
          value: selected,
          onChanged: (bool? value) {
            setState(() {
              if (value!) {
                widget._selectedServers.add(sid);
              } else {
                widget._selectedServers.remove(sid);
              }
            });
          });
      final buyButton = FlatButtonEx.notFilled(
        text: 'buy'.tr(),
        onPressed: () {
          final fetcher = locator<Fetcher>();
          final profile = ProfileInfo.of(context)!.profile!;
          fetcher.launchUrlEx('/package/${server.id}/checkout/${profile.id}');
        },
      );
      final row = Row(
        children: [
          Expanded(
            flex: 4,
            child: checkbox,
          ),
          Expanded(
            child: buyButton,
          ),
        ],
      );
      servers.add(row);
    });

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [_header()] + servers,
    );
  }

  void _selectAll() {
    setState(() {
      _selectedAll = !_selectedAll;
      widget._selectedServers.clear();
      for (int i = 0; i < _allServers.length; ++i) {
        final server = _allServers[i];
        if (_selectedAll) {
          widget._selectedServers.add(server.id!);
        } else {
          widget._selectedServers.remove(server.id);
        }
      }
    });
  }
}
