import 'package:easy_localization/easy_localization.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';

class SubscriberSource extends SortableDataSource<Subscriber> {
  SubscriberSource({required List<DataEntry<Subscriber>> items}) : super(items: items);

  @override
  String get itemsName {
    return 'subscribers';
  }

  @override
  bool searchCondition(String text, Subscriber sub) {
    final lowerEmail = sub.email.toLowerCase();
    return lowerEmail.contains(text.toLowerCase());
  }

  @override
  Widget get noItems {
    return _NoSubsOnline();
  }

  @override
  List<Widget> headers() {
    return [
      Text('email'.tr()),
      Text('created-date'.tr()),
      Text('exp-date'.tr()),
      Text('status'.tr()),
      Text('devices'.tr())
    ];
  }

  @override
  List<Widget> tiles(Subscriber s) {
    return [
      Text(s.email),
      Text(TimeParser.msecHumanReadableFormat(s.createdDate!)),
      Text(TimeParser.msecHumanReadableFormat(s.expDate)),
      Text(s.status.toHumanReadable()),
      Text('${s.maxDevicesCount} (${s.devicesCount})')
    ];
  }

  @override
  bool equalItemsCondition(Subscriber item, Subscriber listItem) {
    return listItem.id == item.id;
  }

  @override
  int compareItems(Subscriber a, Subscriber b, int index) {
    switch (index) {
      case 1:
        return a.email.compareTo(b.email);
      case 2:
        return a.createdDate!.compareTo(b.createdDate!);
      case 3:
        return a.expDate.compareTo(b.expDate);
      case 4:
        return a.status.toHumanReadable().compareTo(b.status.toHumanReadable());
      case 5:
        return a.maxDevicesCount.compareTo(b.maxDevicesCount);
      default:
        throw ArgumentError.value(index, "index");
    }
  }
}

class _NoSubsOnline extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const Center(
        child: NonAvailableBuffer(icon: Icons.error, message: 'Please create Subscribers'));
  }
}
