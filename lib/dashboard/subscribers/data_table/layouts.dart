import 'package:easy_localization/easy_localization.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:frontend/common/scrollbar.dart';
import 'package:frontend/dashboard/subscribers/data_table/data_source.dart';
import 'package:frontend/dashboard/subscribers/devices_dialog.dart';
import 'package:frontend/dashboard/subscribers/subscriber_dialog.dart';
import 'package:frontend/dashboard/subscribers/subscribers_loader.dart';
import 'package:frontend/event_bus.dart';
import 'package:frontend/events/subscriber_events.dart';
import 'package:frontend/models/widgets/common_action_ott_icon.dart';
import 'package:frontend/service_locator.dart';

class SubscribersListLayout extends StatefulWidget {
  final SubscriberSource dataSource;
  final SubscribersLoader loader;

  const SubscribersListLayout(this.dataSource, this.loader);

  @override
  _SubscribersListLayoutState createState() {
    return _SubscribersListLayoutState();
  }
}

class _SubscribersListLayoutState extends State<SubscribersListLayout> {
  @override
  Widget build(BuildContext context) {
    return ScrollbarEx(builder: (controller) {
      return ListView(controller: controller, children: [
        DataLayout<Subscriber>.withHeader(
            dataSource: widget.dataSource,
            header: _header(),
            singleItemActions: singleStreamActions,
            multipleItemActions: multipleStreamActions,
            title: 'subscribers'.tr())
      ]);
    });
  }

  List<IconButton> singleStreamActions(Subscriber sub) {
    return [
      IconButton(
          icon: const Icon(Icons.devices), tooltip: 'devices'.tr(), onPressed: () => _devices(sub)),
      CommonActionOttIcon.edit(() {
        _edit(sub);
      }),
      CommonActionOttIcon.remove(() {
        _remove([sub]);
      })
    ];
  }

  List<IconButton> multipleStreamActions(List<Subscriber> subs) {
    return [
      CommonActionOttIcon.remove(() {
        _remove(subs);
      })
    ];
  }

  // private:
  Widget _header() {
    return DataTableSearchHeader(
        source: widget.dataSource,
        actions: <Widget>[FlatButtonEx.filled(text: 'add'.tr(), onPressed: () => _add())]);
  }

  void _add() {
    showDialog(context: context, builder: (context) => SubscriberDialog.add(widget.loader));
  }

  void _devices(Subscriber sub) {
    showDialog(context: context, builder: (context) => DevicesDialog(sub.id!));
  }

  void _edit(Subscriber sub) {
    showDialog(context: context, builder: (context) => SubscriberDialog.edit(widget.loader, sub));
  }

  void _remove(List<Subscriber> subscribers) {
    for (final subscriber in subscribers) {
      final future = widget.loader.removeSubscriberByID(subscriber.id!);
      future.then((response) {
        final eventBus = locator<FastoEventBus>();
        eventBus.publish(SubscriberRemoveEvent(subscriber));
      }, onError: (error) {
        showError(context, error);
      });
    }
  }
}
