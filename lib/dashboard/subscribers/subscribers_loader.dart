import 'dart:async';

import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/http_response.dart';
import 'package:flutter_common/loader.dart';
import 'package:frontend/fetcher.dart';
import 'package:frontend/service_locator.dart';
import 'package:http/http.dart';

class SubscribersLoader extends ItemBloc {
  @override
  Future<SubscribersDataState> loadDataRoute() {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchGet('/provider/subscribers');
    return response.then((value) {
      final respData = httpDataResponseFromString(value.body);
      final data = respData!.contentMap()!;

      final List<Subscriber> result = [];
      data['subscribers'].forEach((s) {
        final res = Subscriber.fromJson(s);
        result.add(res);
      });
      return SubscribersDataState(result);
    }, onError: (error) {
      throw error;
    });
  }

  Future<Response> removeSubscriberByID(String sid) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchDelete('/subscriber/remove/$sid', [200]);
    return response;
  }

  Future<Subscriber> addSubscriber(Subscriber subscriber) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchPost('/subscriber/add', subscriber.toJson(), [200]);
    return response.then((value) {
      final respData = httpDataResponseFromString(value.body);
      final data = respData!.contentMap()!;
      return Subscriber.fromJson(data);
    }, onError: (error) {
      throw error;
    });
  }

  Future<Subscriber> editSubscriber(Subscriber subscriber) {
    final fetcher = locator<Fetcher>();
    final response =
        fetcher.fetchPatch('/subscriber/edit/${subscriber.id}', subscriber.toJson(), [200]);
    return response.then((value) {
      final respData = httpDataResponseFromString(value.body);
      final data = respData!.contentMap()!;
      return Subscriber.fromJson(data);
    }, onError: (error) {
      throw error;
    });
  }
}

class SubscribersDataState extends ItemDataState<List<Subscriber>> {
  SubscribersDataState(List<Subscriber> data) : super(data);
}

class SubscribersLoaderInfo extends InheritedWidget {
  const SubscribersLoaderInfo({required this.loader, required Widget child}) : super(child: child);

  final SubscribersLoader loader;

  static SubscribersLoader of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<SubscribersLoaderInfo>()!.loader;
  }

  @override
  bool updateShouldNotify(SubscribersLoaderInfo old) => true;
}
