import 'package:easy_localization/easy_localization.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';

class DevicesSource extends SortableDataSource<Device> {
  DevicesSource({required List<DataEntry<Device>> items}) : super(items: items);

  @override
  String get itemsName {
    return 'devices';
  }

  @override
  bool searchCondition(String text, Device dev) {
    final lowerEmail = dev.name.toLowerCase();
    return lowerEmail.contains(text.toLowerCase());
  }

  @override
  Widget get noItems {
    return _NoDevice();
  }

  @override
  List<Widget> headers() {
    return [
      Text('id'.tr()),
      Text('name'.tr()),
      Text('status'.tr()),
      Text('created-date'.tr())
    ];
  }

  @override
  List<Widget> tiles(Device s) {
    return [
      Text(s.id!),
      Text(s.name),
      Text(s.status.toHumanReadable()),
      Text(TimeParser.msecHumanReadableFormat(s.createdDate!))
    ];
  }

  @override
  bool equalItemsCondition(Device item, Device listItem) {
    return listItem.id == item.id;
  }

  @override
  int compareItems(Device a, Device b, int index) {
    switch (index) {
      case 1:
        return a.id!.compareTo(b.id!);
      case 2:
        return a.name.compareTo(b.name);
      case 3:
        return a.status.toHumanReadable().compareTo(b.status.toHumanReadable());
      case 4:
        return a.createdDate!.compareTo(b.createdDate!);
      default:
        throw ArgumentError.value(index, "index");
    }
  }
}

class _NoDevice extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const Center(
        child: NonAvailableBuffer(icon: Icons.error, message: 'Please create Device'));
  }
}
