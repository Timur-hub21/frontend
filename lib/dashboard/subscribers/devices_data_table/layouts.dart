import 'package:easy_localization/easy_localization.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:frontend/common/scrollbar.dart';
import 'package:frontend/dashboard/subscribers/device_dialog.dart';
import 'package:frontend/dashboard/subscribers/devices_data_table/data_source.dart';
import 'package:frontend/dashboard/subscribers/devices_loader.dart';
import 'package:frontend/fetcher.dart';
import 'package:frontend/models/widgets/common_action_ott_icon.dart';
import 'package:frontend/service_locator.dart';

class DevicesListLayout extends StatefulWidget {
  final String sid;
  final DevicesSource dataSource;
  final DevicesLoader loader;

  const DevicesListLayout(this.sid, this.dataSource, this.loader);

  @override
  _DevicesListLayoutState createState() {
    return _DevicesListLayoutState();
  }
}

class _DevicesListLayoutState extends State<DevicesListLayout> {
  @override
  Widget build(BuildContext context) {
    return ScrollbarEx(builder: (controller) {
      return ListView(controller: controller, children: [
        DataLayout<Device>(
            dataSource: widget.dataSource,
            header: _header(),
            singleItemActions: singleStreamActions,
            multipleItemActions: multipleStreamActions)
      ]);
    });
  }

  List<IconButton> singleStreamActions(Device d) {
    return [
      _playlistButton(d),
      CommonActionOttIcon.edit(() {
        _edit(d);
      }),
      CommonActionOttIcon.remove(() {
        _remove([d]);
      })
    ];
  }

  List<IconButton> multipleStreamActions(List<Device> d) {
    return [
      CommonActionOttIcon.remove(() {
        _remove(d);
      }),
    ];
  }

  // private:
  Widget _header() {
    final actions = [FlatButtonEx.filled(text: 'add'.tr(), onPressed: _add)];
    return DataTableSearchHeader(source: widget.dataSource, actions: actions);
  }

  IconButton _playlistButton(Device d) {
    return IconButton(
        icon: const Icon(Icons.video_collection),
        onPressed: () {
          _playlist(d);
        },
        tooltip: 'playlist'.tr());
  }

  void _add() {
    final future = showDialog(
        context: context,
        builder: (context) {
          return DeviceDialog.add(widget.loader);
        });
    future.then((device) {
      if (device != null) {
        widget.dataSource.addItem(device);
      }
    });
  }

  Future<bool> _playlist(Device device) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.launchUrlEx('/client/playlist/${widget.sid}/${device.id}.m3u');
    return response;
  }

  void _edit(Device d) {
    final future = showDialog(
        context: context,
        builder: (context) {
          return DeviceDialog.edit(widget.loader, d);
        });
    future.then((device) {
      if (device != null) {
        widget.dataSource.updateItem(device);
      }
    });
  }

  void _remove(List<Device> devices) {
    for (final device in devices) {
      final future = widget.loader.removeSubscriberDeviceByID(device.id!);
      future.then((response) {
        widget.dataSource.removeItem(device);
      }, onError: (error) {
        showError(context, error);
      });
    }
  }
}
