import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_common/loader.dart';
import 'package:frontend/dashboard/subscribers/data_table/data_source.dart';
import 'package:frontend/dashboard/subscribers/data_table/layouts.dart';
import 'package:frontend/dashboard/subscribers/subscribers_loader.dart';
import 'package:frontend/event_bus.dart';
import 'package:frontend/events/subscriber_events.dart';
import 'package:frontend/service_locator.dart';

class SubscribersWidget extends StatefulWidget {
  const SubscribersWidget({Key? key}) : super(key: key);

  @override
  _SubscribersState createState() {
    return _SubscribersState();
  }
}

class _SubscribersState extends State<SubscribersWidget> {
  final SubscriberSource _subscribers = SubscriberSource(items: []);
  final SubscribersLoader _loader = SubscribersLoader();
  late StreamSubscription<SubscriberAddEvent> _add;
  late StreamSubscription<SubscriberEditEvent> _edit;
  late StreamSubscription<SubscriberRemoveEvent> _remove;

  @override
  void initState() {
    super.initState();
    _loader.load();
    _initEvents();
  }

  @override
  void dispose() {
    _disposeEvents();
    _loader.dispose();
    _subscribers.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LoaderWidget<SubscribersDataState>(
        loader: _loader,
        onState: (state) {
          if (state is SubscribersDataState) {
            _subscribers.clearItems();
            _subscribers.addItems(state.data);
          }
        },
        builder: (_, __) => SubscribersListLayout(_subscribers, _loader));
  }

  void _initEvents() {
    final eventBus = locator<FastoEventBus>();
    _add =
        eventBus.subscribe<SubscriberAddEvent>().listen((event) => _subscribers.addItem(event.sub));
    _edit = eventBus
        .subscribe<SubscriberEditEvent>()
        .listen((event) => _subscribers.updateItem(event.sub));
    _remove = eventBus
        .subscribe<SubscriberRemoveEvent>()
        .listen((event) => _subscribers.removeItem(event.sub));
  }

  void _disposeEvents() {
    _add.cancel();
    _edit.cancel();
    _remove.cancel();
  }
}
