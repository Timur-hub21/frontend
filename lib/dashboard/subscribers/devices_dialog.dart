import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:frontend/dashboard/subscribers/devices_data_table/data_source.dart';
import 'package:frontend/dashboard/subscribers/devices_data_table/layouts.dart';
import 'package:frontend/dashboard/subscribers/devices_loader.dart';

class DevicesDialog extends StatefulWidget {
  final String sid;

  const DevicesDialog(this.sid);

  @override
  _DevicesDialogState createState() {
    return _DevicesDialogState();
  }
}

class _DevicesDialogState extends State<DevicesDialog> {
  late DevicesLoader _loader;
  final DevicesSource _devices = DevicesSource(items: []);

  @override
  void initState() {
    _loader = DevicesLoader(widget.sid);
    _loader.load();
    super.initState();
  }

  @override
  void dispose() {
    _loader.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        title: Text('devices'.tr()),
        contentPadding: const EdgeInsets.all(9),
        content: SizedBox(
            width: 1200,
            height: 600,
            child: LoaderWidget<DevicesDataState>(
                loader: _loader,
                onState: (state) {
                  if (state is DevicesDataState) {
                    _devices.clearItems();
                    _devices.addItems(state.data);
                  }
                },
                builder: (_, __) => DevicesListLayout(widget.sid, _devices, _loader))),
        actions: [FlatButtonEx.notFilled(text: 'Close', onPressed: _ok)]);
  }

  void _ok() {
    Navigator.of(context).pop();
  }
}
