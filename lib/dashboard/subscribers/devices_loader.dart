import 'dart:async';

import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter_common/http_response.dart';
import 'package:flutter_common/loader.dart';
import 'package:frontend/fetcher.dart';
import 'package:frontend/service_locator.dart';
import 'package:http/http.dart';

class DevicesLoader extends ItemBloc {
  final String sid;

  DevicesLoader(this.sid);

  @override
  Future<DevicesDataState> loadDataRoute() {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchGet('/subscriber/devices/$sid');
    return response.then((value) {
      final respData = httpDataResponseFromString(value.body);
      final data = respData!.contentMap()!;

      final List<Device> result = [];
      data['devices'].forEach((s) {
        final res = Device.fromJson(s);
        result.add(res);
      });
      return DevicesDataState(result);
    }, onError: (error) {
      throw error;
    });
  }

  Future<Response> removeSubscriberDeviceByID(String did) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchDelete('/subscriber/device/$sid/remove/$did');
    return response;
  }

  Future<Device> addSubscriberDevice(Device device) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchPost('/subscriber/device/$sid/add', device.toJson());
    return response.then((value) {
      final respData = httpDataResponseFromString(value.body);
      final data = respData!.contentMap()!;
      return Device.fromJson(data);
    }, onError: (error) {
      throw error;
    });
  }

  Future<Device> editSubscriberDevice(Device device) {
    final fetcher = locator<Fetcher>();
    final response =
        fetcher.fetchPatch('/subscriber/device/$sid/edit/${device.id}', device.toJson());
    return response.then((value) {
      final respData = httpDataResponseFromString(value.body);
      final data = respData!.contentMap()!;
      return Device.fromJson(data);
    }, onError: (error) {
      throw error;
    });
  }

  Future<bool> playlist(String sid, String lbId, String did) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.launchUrlEx('/subscriber/device/playlist/$sid/$lbId/$did.m3u');
    return response;
  }
}

class DevicesDataState extends ItemDataState<List<Device>> {
  DevicesDataState(List<Device> data) : super(data);
}
