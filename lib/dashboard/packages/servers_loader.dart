import 'dart:async';

import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter_common/http_response.dart';
import 'package:flutter_common/loader.dart';
import 'package:frontend/fetcher.dart';
import 'package:frontend/service_locator.dart';
import 'package:http/http.dart';

class ServersLoader extends ItemBloc {
  @override
  Future<ServersDataState> loadDataRoute() {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchGet('/provider/servers');
    return response.then((value) {
      final respData = httpDataResponseFromString(value.body);
      final data = respData!.contentMap()!;

      final List<GoServer> result = [];
      data['servers'].forEach((s) {
        final res = GoServer.fromJson(s);
        result.add(res);
      });
      return ServersDataState(result);
    }, onError: (error) {
      throw error;
    });
  }

  Future<Response> removeServerByID(String sid) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchDelete('/server/remove/$sid', [200]);
    return response;
  }

  Future<GoServer> addServer(GoServer server) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchPost('/server/add', server.toJson(), [200]);
    return response.then((value) {
      final respData = httpDataResponseFromString(value.body);
      final data = respData!.contentMap()!;
      return GoServer.fromJson(data);
    }, onError: (error) {
      throw error;
    });
  }

  Future<GoServer> editServer(GoServer server) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchPatch('/server/edit/${server.id}', server.toJson(), [200]);
    return response.then((value) {
      final respData = httpDataResponseFromString(value.body);
      final data = respData!.contentMap()!;
      return GoServer.fromJson(data);
    }, onError: (error) {
      throw error;
    });
  }
}

class ServersDataState extends ItemDataState<List<GoServer>> {
  ServersDataState(List<GoServer> data) : super(data);
}
