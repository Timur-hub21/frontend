import 'dart:convert';

import 'package:easy_localization/easy_localization.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:frontend/brand.dart';
import 'package:frontend/common/scrollbar.dart';
import 'package:frontend/common/servers/details_button.dart';
import 'package:frontend/dashboard/packages/data_table/data_source.dart';
import 'package:frontend/dashboard/packages/server_dialog.dart';
import 'package:frontend/dashboard/packages/servers_loader.dart';
import 'package:frontend/event_bus.dart';
import 'package:frontend/events/package_events.dart';
import 'package:frontend/models/widgets/common_action_ott_icon.dart';
import 'package:frontend/profile.dart';
import 'package:frontend/service_locator.dart';
import 'package:provider/provider.dart' as prov;

class ServersListLayout extends StatefulWidget {
  final ServerSource dataSource;
  final ServersLoader loader;

  const ServersListLayout(this.dataSource, this.loader);

  @override
  _ServersListLayoutState createState() {
    return _ServersListLayoutState();
  }
}

class _ServersListLayoutState extends State<ServersListLayout> {
  @override
  Widget build(BuildContext context) {
    return ScrollbarEx(builder: (controller) {
      return ListView(controller: controller, children: [
        DataLayout<GoServer>.withHeader(
            dataSource: widget.dataSource,
            header: _header(),
            singleItemActions: singleStreamActions,
            multipleItemActions: multipleStreamActions,
            title: 'packages'.tr())
      ]);
    });
  }

  Widget _header() {
    final List<Widget> _actions = [FlatButtonEx.filled(text: 'add'.tr(), onPressed: _add)];
    return DataTableSearchHeader(source: widget.dataSource, actions: _actions);
  }

  List<Widget> singleStreamActions(GoServer server) {
    final _role = ProfileInfo.of(context)!.profile!.type;
    if (_role == ProviderType.ADMIN) {
      return [
        _detailsButton(server),
        CommonActionOttIcon.edit(() => _edit(server)),
        CommonActionOttIcon.remove(() => _remove([server])),
      ];
    }
    return [_detailsButton(server)];
  }

  List<Widget> multipleStreamActions(List<GoServer> servers) {
    final _role = ProfileInfo.of(context)!.profile!.type;
    if (_role != ProviderType.ADMIN) {
      return [];
    }
    return [CommonActionOttIcon.remove(() => _remove(servers))];
  }

  Widget _detailsButton(GoServer s) {
    return DetailsButton(() => _toDetails(s));
  }

  void _edit(GoServer server) {
    showDialog(context: context, builder: (context) => ServerDialog.edit(widget.loader, server));
  }

  void _add() {
    final brand = prov.Provider.of<BrandInfo>(context, listen: false);
    showDialog(
        context: context,
        builder: (context) => ServerDialog.add(widget.loader, brand.brand.backgroundUrl));
  }

  void _remove(List<GoServer> servers) {
    for (final server in servers) {
      final future = widget.loader.removeServerByID(server.id!);
      future.then((response) {
        final eventBus = locator<FastoEventBus>();
        eventBus.publish(PackageRemoveEvent(server));
      }, onError: (error) {
        showError(context, error);
      });
    }
  }

  void _toDetails(GoServer s) {
    // RouterDelegateEx.of(context).setNewRoutePath(DetailsRouteConfig.servers(s.id!));
    final brand = prov.Provider.of<BrandInfo>(context, listen: false).brand;
    final locale = context.locale;
    final scheme = s.host.urlScheme();
    if (scheme == null) {
      return;
    }

    String query = '/?url=${s.host.url}&theme=${brand.theme.toInt()}&locale=${locale.languageCode}';
    final auth = s.host.encodedAuth();
    if (auth != null) {
      query += '&secret=$auth';
    }

    final stabled = base64Encode(utf8.encode(query));
    final url = '$scheme://ws.fastocloud.com/#/?url=$stabled';
    launchExternalUrl(url);
  }
}
