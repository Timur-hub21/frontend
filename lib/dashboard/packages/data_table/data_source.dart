import 'package:easy_localization/easy_localization.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';

class ServerSource extends SortableDataSource<GoServer> {
  ServerSource({required List<DataEntry<GoServer>> items}) : super(items: items);

  @override
  String get itemsName {
    return 'servers';
  }

  @override
  bool searchCondition(String text, GoServer server) {
    return server.name.toLowerCase().contains(text.toLowerCase());
  }

  @override
  Widget get noItems {
    return _NoServers();
  }

  @override
  List<Widget> headers() {
    return [Text('first-name'.tr()), Text('url'.tr())];
  }

  @override
  List<Widget> tiles(GoServer s) {
    return [
      Text(s.name),
      Text(s.host.url.toString()),
    ];
  }

  @override
  bool equalItemsCondition(GoServer item, GoServer listItem) {
    return listItem.id == item.id;
  }

  @override
  int compareItems(GoServer a, GoServer b, int index) {
    switch (index) {
      case 1:
        return a.name.compareTo(b.name);
      case 2:
        return a.host.toString().compareTo(b.host.toString());
      default:
        throw ArgumentError.value(index, "index");
    }
  }
}

class _NoServers extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const Center(
        child: NonAvailableBuffer(icon: Icons.error, message: 'Please create Servers'));
  }
}
