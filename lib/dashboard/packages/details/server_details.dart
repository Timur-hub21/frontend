import 'package:flutter/material.dart';

class ServerDetailsPage extends StatefulWidget {
  final String sid;

  const ServerDetailsPage(this.sid);

  @override
  _ServerDetailsPageState createState() {
    return _ServerDetailsPageState();
  }
}

class _ServerDetailsPageState extends State<ServerDetailsPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return const SizedBox();
  }
}
