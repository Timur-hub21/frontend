import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_common/loader.dart';
import 'package:frontend/dashboard/packages/data_table/data_source.dart';
import 'package:frontend/dashboard/packages/data_table/layouts.dart';
import 'package:frontend/dashboard/packages/servers_loader.dart';
import 'package:frontend/event_bus.dart';
import 'package:frontend/events/package_events.dart';
import 'package:frontend/service_locator.dart';

class ServersPage extends StatefulWidget {
  const ServersPage({Key? key}) : super(key: key);

  @override
  _ServersPageState createState() {
    return _ServersPageState();
  }
}

class _ServersPageState extends State<ServersPage> {
  final ServerSource _servers = ServerSource(items: []);
  final ServersLoader _loader = ServersLoader();
  late StreamSubscription<PackageAddEvent> _add;
  late StreamSubscription<PackageEditEvent> _edit;
  late StreamSubscription<PackageRemoveEvent> _remove;

  @override
  void initState() {
    super.initState();
    _loader.load();
    _initEvents();
  }

  @override
  void dispose() {
    _disposeEvents();
    _loader.dispose();
    _servers.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LoaderWidget<ServersDataState>(
        loader: _loader,
        onState: (state) {
          if (state is ServersDataState) {
            _servers.clearItems();
            _servers.addItems(state.data);
          }
        },
        builder: (_, ServersDataState state) {
          return ServersListLayout(_servers, _loader);
        });
  }

  void _initEvents() {
    final eventBus = locator<FastoEventBus>();
    _add = eventBus.subscribe<PackageAddEvent>().listen((event) {
      _servers.addItem(event.server);
    });
    _edit = eventBus.subscribe<PackageEditEvent>().listen((event) {
      _servers.updateItem(event.server);
    });
    _remove = eventBus.subscribe<PackageRemoveEvent>().listen((event) {
      _servers.removeItem(event.server);
    });
  }

  void _disposeEvents() {
    _add.cancel();
    _edit.cancel();
    _remove.cancel();
  }
}
