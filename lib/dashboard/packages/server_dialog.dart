import 'package:easy_localization/easy_localization.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:fastotv_dart/commands_info/types.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:frontend/common/add_edit_dialog.dart';
import 'package:frontend/common/streams/add_edit/sections/common.dart';
import 'package:frontend/dashboard/packages/servers_loader.dart';
import 'package:frontend/event_bus.dart';
import 'package:frontend/events/package_events.dart';
import 'package:frontend/models/widgets/base.dart';
import 'package:frontend/service_locator.dart';

class ServerDialog extends StatefulWidget {
  final GoServer init;
  final ServersLoader loader;

  ServerDialog.add(this.loader, String background) : init = GoServer.createDefault(background);

  ServerDialog.edit(this.loader, GoServer server) : init = server.copy();

  @override
  _ServerDialogState createState() {
    return _ServerDialogState();
  }
}

class _ServerDialogState extends State<ServerDialog> {
  late GoServer _server;

  bool isAdd() {
    return _server.id == null;
  }

  @override
  void initState() {
    super.initState();
    _server = widget.init;
  }

  @override
  Widget build(BuildContext context) {
    return AddEditDialog.wide(
        add: isAdd(),
        children: <Widget>[
          Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                _nameField(),
                _backgroundUrlField(),
                OptionalFieldTile(
                    title: 'node'.tr(),
                    init: !_server.local,
                    onChanged: (state) {
                      _server.local = !state;
                    },
                    builder: () {
                      return _wsServerField('URL', _server.host);
                    }),
                _header('options'.tr()),
                Row(children: <Widget>[Expanded(child: _descriptionField())]),
                Row(children: <Widget>[Expanded(child: _pricePackage())]),
                Row(children: <Widget>[Expanded(child: _visibleField())]),
              ])
        ],
        onSave: _save);
  }

  Widget _header(String text) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Text(text, style: const TextStyle(fontWeight: FontWeight.bold)));
  }


  Widget _descriptionField() {
    return TextFieldEx(
        hintText: 'package-description'.tr(),
        init: _server.description,
        onFieldChanged: (term) {
          _server.description = term;
        });
  }

  Widget _backgroundUrlField() {
    return TextFieldEx(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        minSymbols: TrailerUrl.MIN_LENGTH,
        maxSymbols: TrailerUrl.MAX_LENGTH,
        hintText: '${'background'.tr()} URL',
        errorText: '${'enter'.tr()} ${'background'.tr()} URL',
        init: _server.backgroundUrl,
        onFieldChanged: (term) {
          _server.backgroundUrl = term;
        });
  }

  Widget _visibleField() {
    return StateCheckBox(
        title: 'visible-for-subscribers'.tr(),
        init: _server.visible,
        onChanged: (value) {
          _server.visible = value;
        });
  }

  Widget _pricePackage() {
    return OptionalFieldTile(
        title: 'price'.tr(),
        init: _server.pricePackage != null,
        onChanged: (state) {
          _server.pricePackage = state
              ? PricePack(price: 1.0, type: PriceType.LIFE_TIME, currency: Currency.USD)
              : null;
        },
        builder: () {
          //TODO(Maxim): figure out, fix and remove this conditions
          if (_server.pricePackage?.currency.type == Currency.USD.type) {
            _server.pricePackage?.currency = const Currency(Currency.CURRENCY_USD);
          } else if (_server.pricePackage?.currency.type == Currency.RUB.type) {
            _server.pricePackage?.currency = const Currency(Currency.CURRENCY_RUB);
          } else if (_server.pricePackage?.currency.type == Currency.BYN.type) {
            _server.pricePackage?.currency = const Currency(Currency.CURRENCY_BYN);
          }
          return Row(children: [
            Expanded(
                child: NumberTextField.decimal(
                    minDouble: Price.MIN,
                    maxDouble: Price.MAX,
                    hintText: 'Price package',
                    canBeEmpty: false,
                    initDouble: _server.pricePackage!.price,
                    onFieldChangedDouble: (term) {
                      if (term != null) {
                        _server.pricePackage!.price = term;
                      }
                    })),
            Expanded(
              child: DropdownButtonEx<Currency>(
                value: _server.pricePackage!.currency,
                values: Currency.values,
                onChanged: (value) {
                  setState(() {
                    _server.pricePackage!.currency = value;
                  });
                },
                itemBuilder: (Currency value) {
                  return DropdownMenuItem<Currency>(
                      child: Text(value.toHumanReadable()), value: value);
                },
              ),
            ),
            Expanded(
                child: DropdownButtonEx<PriceType>(
                    value: _server.pricePackage!.type,
                    values: PriceType.values,
                    onChanged: (c) {
                      setState(() {
                        _server.pricePackage!.type = c;
                      });
                    },
                    itemBuilder: (PriceType data) {
                      return DropdownMenuItem(child: Text(data.toHumanReadable()), value: data);
                    }))
          ]);
        });
  }

  Widget _nameField() {
    return TextFieldEx(
        hintText: 'name'.tr(),
        errorText: '${'enter'.tr()} ${'name'.tr()}',
        init: _server.name,
        onFieldChanged: (term) {
          _server.name = term;
        });
  }

  // field builders
  Widget _wsServerField(String hint, WSServer init) {
    return WSServerTextField(urlHint: hint, urlError: '${'enter'.tr()} host URL', value: init);
  }

  void _save() {
    if (!_server.isValid()) {
      return;
    }

    final Future<GoServer> _response =
        isAdd() ? widget.loader.addServer(_server) : widget.loader.editServer(_server);
    _response.then((GoServer server) {
      final eventBus = locator<FastoEventBus>();
      isAdd()
          ? eventBus.publish(PackageAddEvent(server))
          : eventBus.publish(PackageEditEvent(server));
      Navigator.of(context).pop();
    }, onError: (error) {
      showError(context, error);
    });
  }
}
