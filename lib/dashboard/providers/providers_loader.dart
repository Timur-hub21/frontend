import 'dart:async';

import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter_common/http_response.dart';
import 'package:flutter_common/loader.dart';
import 'package:frontend/fetcher.dart';
import 'package:frontend/service_locator.dart';
import 'package:http/http.dart';

class ProvidersLoader extends ItemBloc {
  @override
  Future<ProvidersDataState> loadDataRoute() {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchGet('/provider/providers');
    return response.then((value) {
      final respData = httpDataResponseFromString(value.body);
      final data = respData!.contentMap()!;

      final List<Provider> result = [];
      data['providers'].forEach((s) {
        final res = Provider.fromJson(s);
        result.add(res);
      });
      return ProvidersDataState(result);
    }, onError: (error) {
      throw error;
    });
  }

  Future<Response> createProvider(Provider provider) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchPost('/signup', provider.toJson(), [200]);
    return response;
  }

  Future<Response> removeProviderByID(String sid) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchDelete('/provider/remove/$sid', [200]);
    return response;
  }

  Future<Provider> addProvider(Provider provider) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchPost('/provider/add', provider.toJson(), [200]);
    return response.then((value) {
      final respData = httpDataResponseFromString(value.body);
      final data = respData!.contentMap()!;
      return Provider.fromJson(data);
    }, onError: (error) {
      throw error;
    });
  }

  Future<Provider> editProvider(Provider provider) {
    final fetcher = locator<Fetcher>();
    final response = fetcher.fetchPatch('/provider/edit/${provider.id}', provider.toJson(), [200]);
    return response.then((value) {
      final respData = httpDataResponseFromString(value.body);
      final data = respData!.contentMap()!;
      return Provider.fromJson(data);
    }, onError: (error) {
      throw error;
    });
  }
}

class ProvidersDataState extends ItemDataState<List<Provider>> {
  ProvidersDataState(List<Provider> data) : super(data);
}
