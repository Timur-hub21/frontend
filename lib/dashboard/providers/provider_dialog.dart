import 'package:easy_localization/easy_localization.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:frontend/common/add_edit_dialog.dart';
import 'package:frontend/common/locale_picker.dart';
import 'package:frontend/common/scrollbar.dart';
import 'package:frontend/dashboard/providers/providers_loader.dart';
import 'package:frontend/event_bus.dart';
import 'package:frontend/events/provider_events.dart';
import 'package:frontend/service_locator.dart';

class ProviderDialog extends StatelessWidget {
  final Provider init;
  final ProvidersLoader loader;

  ProviderDialog.add(this.loader) : init = Provider.createDefault();

  ProviderDialog.edit(this.loader, Provider provider) : init = provider.copy()..password = null;

  bool get isAdd => init.id == null;

  @override
  Widget build(BuildContext context) {
    return AddEditDialog.narrow(
        add: isAdd,
        children: <Widget>[
          _Fields(init, isAdd),
          _StatusPicker(init),
          _TypePicker(init),
          _LocalePickers(init)
        ],
        onSave: () {
          _save(context);
        });
  }

  void _save(BuildContext context) {
    if (!init.isValid()) {
      return;
    }

    final Future<Provider> _response = isAdd ? loader.addProvider(init) : loader.editProvider(init);
    _response.then((Provider provider) {
      final eventBus = locator<FastoEventBus>();
      isAdd
          ? eventBus.publish(ProviderAddEvent(provider))
          : eventBus.publish(ProviderEditEvent(provider));
      Navigator.of(context).pop();
    }, onError: (error) {
      showError(context, error);
    });
  }
}

class _Fields extends StatelessWidget {
  const _Fields(this.prov, this.emptyPassword);

  final Provider prov;
  final bool emptyPassword;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          TextFieldEx(
              hintText: 'first-name'.tr(),
              errorText: '${'enter'.tr()} ${'first-name'.tr()}',
              minSymbols: Provider.MIN_NAME,
              maxSymbols: Provider.MAX_NAME,
              init: prov.firstName,
              onFieldChanged: (term) {
                prov.firstName = term;
              }),
          TextFieldEx(
              hintText: 'last-name'.tr(),
              errorText: '${'enter'.tr()} ${'last-name'.tr()}',
              minSymbols: Provider.MIN_NAME,
              maxSymbols: Provider.MAX_NAME,
              init: prov.lastName,
              onFieldChanged: (term) {
                prov.lastName = term;
              }),
          TextFieldEx(
              hintText: 'email'.tr(),
              errorText: '${'enter-your'.tr()} ${'email'.tr()}',
              init: prov.email,
              onFieldChanged: (term) {
                prov.email = term;
              },
              keyboardType: TextInputType.emailAddress),
          PassWordTextField(
              hintText: 'password'.tr(),
              errorText: emptyPassword ? '${'enter'.tr()} ${'password'.tr()}' : null,
              onFieldChanged: (term) {
                prov.password = term;
              }),
          NumberTextField.integer(
              minInt: Credits.MIN,
              maxInt: Credits.MAX,
              hintText: 'credits'.tr(),
              canBeEmpty: false,
              initInt: prov.credits,
              onFieldChangedInt: (term) {
                if (term != null) prov.credits = term;
              })
        ]));
  }
}

class _StatusPicker extends DropdownButtonEx<ProviderStatus> {
  _StatusPicker(Provider prov)
      : super(
            hint: prov.status.toHumanReadable(),
            value: prov.status,
            values: ProviderStatus.values,
            onChanged: (c) {
              prov.status = c;
            },
            itemBuilder: (ProviderStatus value) {
              return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value);
            });
}

class _TypePicker extends DropdownButtonEx<ProviderType> {
  _TypePicker(Provider prov)
      : super(
            hint: prov.type.toHumanReadable(),
            value: prov.type,
            values: ProviderType.values,
            onChanged: (c) {
              prov.type = c;
            },
            itemBuilder: (ProviderType value) {
              return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value);
            });
}

class _LocalePickers extends StatelessWidget {
  const _LocalePickers(this.prov) : super(key: const ValueKey('Lang picker'));

  final Provider prov;

  @override
  Widget build(BuildContext context) {
    return LocalesPicker(
        onInit: (country, language) {
          prov.country ??= country;
          prov.language ??= language;
          ScrollbarEx.of(context)!.refresh();
        },
        onCountryChanged: (country) => prov.country = country,
        onLanguageChanged: (lang) => prov.language = lang);
  }
}
