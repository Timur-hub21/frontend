import 'package:easy_localization/easy_localization.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:frontend/common/scrollbar.dart';
import 'package:frontend/dashboard/providers/data_table/data_source.dart';
import 'package:frontend/dashboard/providers/provider_dialog.dart';
import 'package:frontend/dashboard/providers/providers_loader.dart';
import 'package:frontend/event_bus.dart';
import 'package:frontend/events/provider_events.dart';
import 'package:frontend/models/widgets/common_action_ott_icon.dart';
import 'package:frontend/service_locator.dart';

class ProvidersListLayout extends StatefulWidget {
  final ProviderSource dataSource;
  final ProvidersLoader loader;

  const ProvidersListLayout(this.dataSource, this.loader);

  @override
  _ProvidersListLayoutState createState() {
    return _ProvidersListLayoutState();
  }
}

class _ProvidersListLayoutState extends State<ProvidersListLayout> {
  @override
  Widget build(BuildContext context) {
    return ScrollbarEx(builder: (controller) {
      return ListView(controller: controller, children: [
        DataLayout<Provider>.withHeader(
            dataSource: widget.dataSource,
            header: _header(),
            singleItemActions: singleStreamActions,
            multipleItemActions: multipleStreamActions,
            title: 'providers'.tr())
      ]);
    });
  }

  Widget _header() {
    return DataTableSearchHeader(
        source: widget.dataSource,
        actions: <Widget>[FlatButtonEx.filled(text: 'add'.tr(), onPressed: _add)]);
  }

  List<IconButton> singleStreamActions(Provider p) {
    return [
      CommonActionOttIcon.edit(() => edit(p)),
      CommonActionOttIcon.remove(() => remove([p])),
    ];
  }

  List<IconButton> multipleStreamActions(List<Provider> p) {
    return [CommonActionOttIcon.remove(() => remove(p))];
  }

  void _add() {
    showDialog(context: context, builder: (context) => ProviderDialog.add(widget.loader));
  }

  void edit(Provider provider) {
    showDialog(
        context: context, builder: (context) => ProviderDialog.edit(widget.loader, provider));
  }

  void remove(List<Provider> providers) {
    for (final provider in providers) {
      final future = widget.loader.removeProviderByID(provider.id!);
      future.then((response) {
        final eventBus = locator<FastoEventBus>();
        eventBus.publish(ProviderRemoveEvent(provider));
      }, onError: (error) {
        showError(context, error);
      });
    }
  }
}
