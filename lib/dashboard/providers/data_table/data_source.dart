import 'package:easy_localization/easy_localization.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';

String _date(int milliseconds) => DateTime.fromMillisecondsSinceEpoch(milliseconds).toString();

class ProviderSource extends SortableDataSource<Provider> {
  ProviderSource({required List<DataEntry<Provider>> items}) : super(items: items);

  @override
  String get itemsName => 'providers';

  @override
  bool searchCondition(String text, Provider sub) {
    return sub.email.toLowerCase().contains(text.toLowerCase());
  }

  @override
  Widget get noItems => _NoSubsOnline();

  @override
  List<Widget> headers() {
    return [
      Text('email'.tr()),
      Text('created-date'.tr()),
      Text('status'.tr()),
      Text('type'.tr())
    ];
  }

  @override
  List<Widget> tiles(Provider s) => [
        Text(s.email),
        Text(_date(s.createdDate!)),
        Text(s.status.toHumanReadable()),
        Text(s.type.toHumanReadable())
      ];

  @override
  bool equalItemsCondition(Provider item, Provider listItem) {
    return listItem.id == item.id;
  }

  @override
  int compareItems(Provider a, Provider b, int index) {
    switch (index) {
      case 1:
        return a.email.compareTo(b.email);
      case 2:
        return a.createdDate!.compareTo(b.createdDate!);
      case 3:
        return a.status.toHumanReadable().compareTo(b.status.toHumanReadable());
      case 4:
        return a.type.toHumanReadable().compareTo(b.type.toHumanReadable());
      default:
        throw ArgumentError.value(index, "index");
    }
  }
}

class _NoSubsOnline extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const Center(
        child: NonAvailableBuffer(icon: Icons.error, message: 'Please create Providers'));
  }
}
