import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_common/loader.dart';
import 'package:frontend/dashboard/providers/data_table/data_source.dart';
import 'package:frontend/dashboard/providers/data_table/layouts.dart';
import 'package:frontend/dashboard/providers/providers_loader.dart';
import 'package:frontend/event_bus.dart';
import 'package:frontend/events/provider_events.dart';
import 'package:frontend/service_locator.dart';

class ProvidersWidget extends StatefulWidget {
  const ProvidersWidget({Key? key}) : super(key: key);

  @override
  _ProvidersState createState() {
    return _ProvidersState();
  }
}

class _ProvidersState extends State<ProvidersWidget> {
  final ProviderSource _providers = ProviderSource(items: []);
  final ProvidersLoader _loader = ProvidersLoader();
  late StreamSubscription<ProviderAddEvent> _add;
  late StreamSubscription<ProviderEditEvent> _edit;
  late StreamSubscription<ProviderRemoveEvent> _remove;

  @override
  void initState() {
    super.initState();
    _loader.load();
    _initEvents();
  }

  @override
  void dispose() {
    _disposeEvents();
    _loader.dispose();
    _providers.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LoaderWidget<ProvidersDataState>(
        loader: _loader,
        onState: (state) {
          if (state is ProvidersDataState) {
            _providers.clearItems();
            _providers.addItems(state.data);
          }
        },
        builder: (_, __) => ProvidersListLayout(_providers, _loader));
  }

  void _initEvents() {
    final eventBus = locator<FastoEventBus>();
    _add = eventBus.subscribe<ProviderAddEvent>().listen((event) => _providers.addItem(event.sub));
    _edit =
        eventBus.subscribe<ProviderEditEvent>().listen((event) => _providers.updateItem(event.sub));
    _remove = eventBus
        .subscribe<ProviderRemoveEvent>()
        .listen((event) => _providers.removeItem(event.sub));
  }

  void _disposeEvents() {
    _add.cancel();
    _edit.cancel();
    _remove.cancel();
  }
}
