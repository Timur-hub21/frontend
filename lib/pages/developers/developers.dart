import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:frontend/constants.dart';
import 'package:frontend/pages/controls.dart';

class DevelopersPage extends StatelessWidget {
  const DevelopersPage();

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
      ClickableText('Installation', () {
        launchExternalUrl(INSTALLATION_MEDIA_PART_URL);
      }, color: Colors.black),
      ClickableText('F.A.Q', () {
        launchExternalUrl(FAQ_URL);
      }, color: Colors.black),
      ClickableText('Media Service API', () {
        launchExternalUrl(MEDIA_API_DOCS_URL);
      }, color: Colors.black),
      ClickableText('Get license key', () {
        launchExternalUrl(LICENSE_GEN_URL);
      }, color: Colors.black)
    ]));
  }
}
