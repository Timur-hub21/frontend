import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/errors.dart';
import 'package:flutter_common/widgets.dart';
import 'package:frontend/brand.dart';
import 'package:frontend/common/locale_picker.dart';
import 'package:frontend/dashboard/providers/providers_loader.dart';
import 'package:frontend/pages/login/layout.dart';
import 'package:frontend/routing/route_config.dart';
import 'package:frontend/routing/router_delegate.dart';
import 'package:provider/provider.dart' as prov;
import 'package:responsive_builder/responsive_builder.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage();

  @override
  // ignore: no_logic_in_create_state
  _SignUpPageState createState() {
    return _SignUpPageState();
  }
}

class _SignUpPageState extends State<SignUpPage> {
  final StreamController<bool?> _loading = StreamController<bool?>();

  bool get _canSignUp => _formKey.currentState!.validate() && _termsAndConditions;

  String _email = '';
  String _password = '';
  String _firstName = '';
  String _lastName = '';
  bool _termsAndConditions = false;
  String _currentCountry = '';
  String _currentLang = '';

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    _loading.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final brand = prov.Provider.of<BrandInfo>(context);
    return LoginLayout(
        content: Padding(
            padding: const EdgeInsets.all(16.0),
            child: StreamBuilder<bool?>(
                initialData: false,
                stream: _loading.stream,
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          const CheckEmail(),
                          const SizedBox(height: 16),
                          FlatButtonEx.filled(text: 'Dismiss', onPressed: _toSignIn)
                        ]);
                  }

                  final bool isLoading = snapshot.data!;
                  return IgnorePointer(
                      ignoring: isLoading,
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            LoginHeader('sign-up'.tr(), brand.brand.logo),
                            Form(
                                key: _formKey,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [_names(), _emailField(), _passwordField()],
                                )),
                            LocalesPicker(
                                onInit: (country, lang) {
                                  _currentCountry = country;
                                  _currentLang = lang;
                                },
                                onCountryChanged: (country) => _currentCountry = country,
                                onLanguageChanged: (lang) => _currentLang = lang),
                            _termsAndConditionsText(),
                            const SizedBox(height: 8),
                            LoginMainButton(
                                loading: isLoading, title: 'sign-up'.tr(), onPressed: _signUp),
                            TextButton(child: Text('have-an-account'.tr()), onPressed: _toSignIn)
                          ]));
                })));
  }

  Widget _names() {
    return ScreenTypeLayout(
        mobile: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[_firstNameField(), _lastNameField()]),
        desktop: Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
          Expanded(child: _firstNameField()),
          Expanded(child: _lastNameField())
        ]));
  }

  Widget _firstNameField() {
    return TextFieldEx(
        autofocus: true,
        hintText: 'first-name'.tr(),
        errorText: 'Enter your first name',
        init: _firstName,
        onFieldChanged: (term) {
          _firstName = term;
        });
  }

  Widget _lastNameField() {
    return TextFieldEx(
        hintText: 'last-name'.tr(),
        errorText: 'Enter your last name',
        init: _lastName,
        onFieldChanged: (term) {
          _lastName = term;
        });
  }

  Widget _emailField() {
    return TextFieldEx(
        hintText: 'email'.tr(),
        errorText: '${'enter-your'.tr()} ${'email'.tr()}',
        init: _email,
        onFieldChanged: (term) {
          _email = term;
        },
        keyboardType: TextInputType.emailAddress);
  }

  Widget _passwordField() {
    return PassWordTextField(
        hintText: 'password'.tr(),
        errorText: '${'enter'.tr()} ${'password'.tr()}',
        onFieldChanged: (term) {
          _password = term;
        },
        init: _password);
  }

  Widget _termsAndConditionsText() {
    return CheckboxListTile(
        title: RichText(
            text: TextSpan(children: [
          TextSpan(text: 'i-have-read'.tr(), style: const TextStyle(color: Colors.black)),
          TextSpan(
              text: 'terms-and-conditions'.tr(),
              style: TextStyle(color: Theme.of(context).colorScheme.secondary),
              recognizer: TapGestureRecognizer()..onTap = _onTerms)
        ])),
        value: _termsAndConditions,
        onChanged: (bool? term) {
          setState(() {
            _termsAndConditions = term!;
          });
        });
  }

  void _toSignIn() {
    RouterDelegateEx.of(context).setNewRoutePath(const RouteConfig.signIn());
  }

  void _onTerms() {
    RouterDelegateEx.of(context).setNewRoutePath(const RouteConfig.terms());
  }

  void _signUp() {
    if (!_canSignUp) {
      return;
    }

    _loading.add(true);
    final email = _email.toLowerCase().trim();
    final Provider _provider = Provider.createDefault()
      ..email = email
      ..password = _password
      ..firstName = _firstName
      ..lastName = _lastName
      ..country = _currentCountry
      ..language = _currentLang;
    ProvidersLoader().createProvider(_provider).then((response) {
      if (response.statusCode == 200) {
        _toSignIn();
      } else {
        _loading.add(false);
      }
    }, onError: (error) {
      showError(context, error);
      _loading.add(false);
    });
  }
}

class CheckEmail extends StatelessWidget {
  const CheckEmail();

  @override
  Widget build(BuildContext context) {
    return const NonAvailableBuffer(
        icon: Icons.mail, message: 'Check your inbox for\nconfirmation message');
  }
}
