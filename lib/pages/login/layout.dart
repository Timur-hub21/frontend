import 'package:flutter/material.dart';
import 'package:frontend/common/logo.dart';
import 'package:frontend/pages/login/border.dart';

class LoginLayout extends StatelessWidget {
  const LoginLayout({required this.content});

  final Widget content;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: Center(
            child: SingleChildScrollView(
                child: Center(
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center, children: [_layouts()])))));
  }

  Widget _layouts() {
    return LayoutBuilder(builder: (context, constraints) {
      if (constraints.maxWidth <= 600) {
        return content;
      }
      return LoginBorderWrap(child: content);
    });
  }
}

class LoginHeader extends StatelessWidget {
  const LoginHeader(this.title, this.logoPath);

  final String title;
  final String logoPath;

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: [
      LogoButton(size: 96, path: logoPath),
      const SizedBox(height: 24),
      Text(title, style: const TextStyle(fontWeight: FontWeight.w500, fontSize: 24)),
      const SizedBox(height: 16)
    ]);
  }
}

class LoginMainButton extends StatelessWidget {
  const LoginMainButton({required this.title, required this.onPressed, this.loading = false});

  final String title;
  final VoidCallback onPressed;
  final bool loading;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(8),
        child: SizedBox(
            width: double.maxFinite,
            height: 48,
            child: ElevatedButton(
                child: loading ? const _Loading() : Text(title), onPressed: onPressed)));
  }
}

class _Loading extends StatelessWidget {
  const _Loading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SizedBox(
        height: 18,
        width: 18,
        child: CircularProgressIndicator(color: Colors.white, strokeWidth: 2));
  }
}
