import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/errors.dart';
import 'package:flutter_common/loader.dart';
import 'package:flutter_common/widgets.dart';
import 'package:frontend/brand.dart';
import 'package:frontend/dashboard/profile/profile_loader.dart';
import 'package:frontend/fetcher.dart';
import 'package:frontend/pages/login/layout.dart';
import 'package:frontend/profile.dart';
import 'package:frontend/routing/route_config.dart';
import 'package:frontend/routing/router_delegate.dart';
import 'package:frontend/service_locator.dart';
import 'package:frontend/shared_prefs.dart';
import 'package:provider/provider.dart' as prov;

class SignInPage extends StatefulWidget {
  final String? token;

  const SignInPage({required this.token});

  @override
  _SignInPageState createState() {
    return _SignInPageState();
  }
}

class _SignInPageState extends State<SignInPage> {
  final StreamController<bool> _loading = StreamController<bool>();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  bool get _canSignIn => _formKey.currentState!.validate();

  bool _initLoading = false;
  String _email = '';
  String _password = '';
  bool _remember = false;

  @override
  void initState() {
    super.initState();
    final settings = locator<LocalStorageService>();
    _remember = settings.check();

    if (widget.token != null) {
      _initLoading = true;
      _confirmEmail();
    } else if (_remember) {
      _email = settings.email() ?? '';
      _password = settings.password() ?? '';
      if (_email.isNotEmpty && _password.isNotEmpty) {
        _initLoading = true;
        _signIn();
      }
    }
  }

  @override
  void dispose() {
    _loading.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final brand = prov.Provider.of<BrandInfo>(context);
    return LoginLayout(
        content: Padding(
            padding: const EdgeInsets.all(16.0),
            child: StreamBuilder<bool>(
                initialData: _initLoading,
                stream: _loading.stream,
                builder: (context, snapshot) {
                  final bool isLoading = snapshot.data!;
                  return IgnorePointer(
                      ignoring: isLoading,
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            LoginHeader('sign-in'.tr(), brand.brand.logo),
                            Form(
                                key: _formKey,
                                child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      TextFieldEx(
                                          hintText: 'email'.tr(),
                                          errorText: '${'enter-your'.tr()} ${'email'.tr()}',
                                          init: _email,
                                          onFieldChanged: (term) => _email = term,
                                          keyboardType: TextInputType.emailAddress,
                                          autofocus: !isLoading),
                                      PassWordTextField(
                                          hintText: 'password'.tr(),
                                          errorText: '${'enter'.tr()} ${'password'.tr()}',
                                          init: _password,
                                          onFieldChanged: (term) => _password = term),
                                      CheckboxListTile(
                                          value: _remember,
                                          onChanged: (bool? term) {
                                            setState(() {
                                              _remember = term!;
                                            });
                                          },
                                          title: Text('remember-me'.tr()),
                                          controlAffinity: ListTileControlAffinity.trailing),
                                      LoginMainButton(
                                          loading: isLoading,
                                          title: 'sign-in'.tr(),
                                          onPressed: () {
                                            if (_canSignIn) {
                                              FocusScope.of(context).unfocus();
                                              _signIn();
                                            }
                                          }),
                                      TextButton(
                                          child: Text('create-account'.tr()),
                                          onPressed: () {
                                            RouterDelegateEx.of(context)
                                                .setNewRoutePath(const RouteConfig.signUp());
                                          })
                                    ]))
                          ]));
                })));
  }

  void _confirmEmail() {
    locator<Fetcher>().fetchGet('/confirm_email/${widget.token}').then((response) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        showDialog(
            context: context,
            builder: (context) {
              return const AlertDialog(
                  title: Text('Email confirmed'), content: Text('Thanks for registration'));
            });
      });
    }, onError: (error) {
      showError(context, error);
    }).whenComplete(() {
      _loading.add(false);
    });
  }

  void _signIn() {
    _loading.add(true);
    final fetcher = locator<Fetcher>();
    final settings = locator<LocalStorageService>();
    settings.setCheck(_remember);
    final email = _email.toLowerCase().trim();
    if (_remember) {
      settings.setEmail(_email);
      settings.setPassword(_password);
    }
    final signinResponse = fetcher.loginEx('/signin', {'email': email, 'password': _password});
    signinResponse.then((response) {
      final ProviderLoader _loader = ProviderLoader();
      _loader.load();
      _loader.stream().listen((state) {
        if (state is ProviderDataState) {
          final Provider profile = state.data;
          ProfileInfo.of(context)!.updateProfile(profile);
          RouterDelegateEx.of(context).toProfile();
        } else if (state is ItemErrorState) {
          showError(context, state.error as ErrorHttp);
          _loading.add(false);
        }
      });
    }, onError: (error) {
      showError(context, error);
      _loading.add(false);
    });
  }
}
