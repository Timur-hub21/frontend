import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:frontend/brand.dart';
import 'package:frontend/profile.dart';
import 'package:frontend/routing/route_config.dart';
import 'package:frontend/routing/router_delegate.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';

class DashboardHeader extends StatefulWidget {
  final Widget child;
  final bool needFooter;

  const DashboardHeader(this.child, this.needFooter);

  @override
  State<DashboardHeader> createState() => _DashboardHeaderState();
}

class _DashboardHeaderState extends State<DashboardHeader> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizingInformation) {
      return Theme(
          data: Theme.of(context),
          child: Scaffold(
            appBar: _header(context, sizingInformation.isDesktop),
            body: widget.child,
            bottomNavigationBar:
                widget.needFooter ? _footer(context, sizingInformation.isDesktop) : null,
          ));
    });
  }

  AppBar _header(BuildContext context, bool isDesktop) {
    final brand = Provider.of<BrandInfo>(context);
    return AppBar(
        automaticallyImplyLeading: false,
        title: Row(children: [_HomeTitle(brand.brand.title)]),
        actions: _headerActions(context, isDesktop));
  }

  BottomAppBar _footer(BuildContext context, bool isDesktop) {
    return BottomAppBar(
      color: Theme.of(context).primaryColor,
      child: _footerActions(isDesktop),
    );
  }

  Widget _footerActions(bool isDesktop) {
    final base = <Widget>[
      _HeaderRouteButton(title: 'terms-and-conditions'.tr(), route: const RouteConfig.terms()),
      _HeaderRouteButton(title: 'privacy-policy'.tr(), route: const RouteConfig.privacy()),
    ];
    return isDesktop
        ? Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: base)
        : Column(mainAxisSize: MainAxisSize.min, children: base);
  }

  List<Widget> _headerActions(BuildContext context, bool isDesktop) {
    final base = [
      _HeaderRouteButton(title: 'downloads'.tr(), route: const RouteConfig.downloads()),
      _HeaderRouteButton(title: 'developers'.tr(), route: const RouteConfig.developers())
    ];
    final isLogged = ProfileInfo.of(context)!.profile != null;
    if (isLogged) {
      return [
        if (isDesktop) ...base,
        _HeaderRouteButton(title: 'profile'.tr(), route: const RouteConfig.profile()),
        _logOutButton(context)
      ];
    }
    return [
      if (isDesktop) ...base,
      _HeaderRouteButton(title: 'sign-in'.tr(), route: const RouteConfig.signIn()),
      const SizedBox(width: 16)
    ];
  }

  Widget _logOutButton(BuildContext context) {
    return IconButton(
        icon: const Icon(Icons.exit_to_app),
        tooltip: '${'sign-in'.tr()}/${'logout'.tr()}',
        onPressed: () {
          _navigate(context, const RouteConfig.home());
          ProfileInfo.of(context)!.logout();
        });
  }

  void _navigate(BuildContext context, RouteConfig route) {
    RouterDelegateEx.of(context).setNewRoutePath(route);
  }
}

class _HeaderRouteButton extends StatelessWidget {
  final String title;
  final RouteConfig route;

  const _HeaderRouteButton({
    Key? key,
    required this.title,
    required this.route,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _HeaderButton(
      title: title,
      onPressed: () => RouterDelegateEx.of(context).setNewRoutePath(route),
    );
  }
}

class _HeaderButton extends StatelessWidget {
  final String title;
  final VoidCallback onPressed;

  const _HeaderButton({
    Key? key,
    required this.title,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final style = TextButton.styleFrom(
        primary: Colors.white, padding: const EdgeInsets.symmetric(horizontal: 22));

    return TextButton(
      style: style,
      child: Text(title, style: const TextStyle(fontWeight: FontWeight.bold)),
      onPressed: onPressed,
    );
  }
}

class _HomeTitle extends StatelessWidget {
  final String title;

  const _HomeTitle(this.title);

  @override
  Widget build(BuildContext context) {
    return InkWell(
        highlightColor: Colors.transparent,
        hoverColor: Colors.transparent,
        splashColor: Colors.transparent,
        child: SizedBox(height: kToolbarHeight, child: Center(child: _title(context, title))),
        onTap: () {
          RouterDelegateEx.of(context).toHome();
        });
  }

  Widget _title(BuildContext context, title) {
    return Text(title,
        style: TextStyle(color: Theme.of(context).appBarTheme.actionsIconTheme!.color));
  }
}
