import 'package:flutter/material.dart';

class ClickableText extends StatelessWidget {
  final String text;
  final void Function() onTap;
  final double size;
  final Color color;

  const ClickableText(this.text, this.onTap, {this.size = 20, this.color = Colors.white});

  @override
  Widget build(BuildContext context) {
    return InkWell(
        highlightColor: Colors.transparent,
        hoverColor: Colors.transparent,
        splashColor: Colors.transparent,
        child: StaticText(text, size: size, color: color),
        onTap: onTap);
  }
}

class StaticText extends StatelessWidget {
  final String title;
  final Color color;
  final double size;

  const StaticText(this.title, {this.size = 20, this.color = Colors.white});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(title, style: TextStyle(color: color, fontSize: size), softWrap: true));
  }
}
