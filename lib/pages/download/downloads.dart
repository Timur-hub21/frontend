import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:frontend/models/product.dart';
import 'package:frontend/pages/download/downloads_loader.dart';
import 'package:responsive_builder/responsive_builder.dart';

class DownloadsPage extends StatefulWidget {
  const DownloadsPage();

  @override
  _DownloadsPageState createState() {
    return _DownloadsPageState();
  }
}

class _DownloadsPageState extends State<DownloadsPage> {
  final DownloadsLoader _loader = DownloadsLoader();

  @override
  void initState() {
    super.initState();
    _loader.load();
  }

  @override
  void dispose() {
    _loader.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LoaderWidget<PackagesDataState>(
        loader: _loader,
        builder: (context, state) {
          final List<Product> products = state.data;
          return ResponsiveBuilder(builder: (context, sizingInformation) {
            if (sizingInformation.isMobile) {
              return ListView(
                  children: List<Widget>.generate(products.length, (index) {
                return ProductCard(products[index]);
              }));
            }
            return SingleChildScrollView(
                child: Center(
                    child: Wrap(
                        spacing: 8,
                        runSpacing: 8,
                        children: List<Widget>.generate(products.length, (index) {
                          return ProductCard(products[index], 300);
                        }))));
          });
        });
  }
}

class ProductCard extends StatelessWidget {
  final Product product;
  final double? width;

  const ProductCard(this.product, [this.width]);

  @override
  Widget build(BuildContext context) {
    return SizedBox(width: width, child: Card(child: _content()));
  }

  // private:
  Widget _content() {
    return ResponsiveBuilder(builder: (context, sizingInformation) {
      final platforms = _platforms();
      if (sizingInformation.isMobile) {
        return ListTile(
            title: Text(product.name),
            subtitle: Text(product.description),
            onTap: () {
              showDialog(
                  context: context,
                  builder: (context) {
                    return SimpleDialog(title: Text('packages'.tr()), children: platforms);
                  });
            });
      }

      platforms.insert(0, _header());
      return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: platforms);
    });
  }

  Widget _header() {
    return Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(product.name, style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
              const Divider(height: 4, thickness: 1),
              Text(product.description)
            ]));
  }

  List<Widget> _platforms() {
    return List<Widget>.generate(product.packages.length, (index) {
      final Package package = product.packages[index];
      final String path = 'install/assets/platforms/${_formatName(package.name)}.png';
      return ListTile(
          leading: CustomAssetLogo(path, height: 48, width: 48),
          title: Text('${package.version} (${package.arch})'),
          subtitle: Text(package.package.version),
          trailing: IconButton(
              tooltip: 'copy-download-link'.tr(),
              icon: const Icon(Icons.copy),
              onPressed: () {
                Clipboard.setData(ClipboardData(text: package.package.url));
              }),
          onTap: () {
            launchExternalUrl(package.package.url);
          });
    });
  }

  String _formatName(String name) {
    return name.toLowerCase().replaceAll(RegExp(' '), '_');
  }
}
