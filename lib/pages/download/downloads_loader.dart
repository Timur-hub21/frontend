import 'dart:async';
import 'dart:convert';

import 'package:flutter_common/loader.dart';
import 'package:frontend/fetcher.dart';
import 'package:frontend/models/product.dart';
import 'package:frontend/service_locator.dart';

class DownloadsLoader extends ItemBloc {
  @override
  Future<PackagesDataState> loadDataRoute() {
    final fetcher = locator<Fetcher>();
    final response = fetcher.getDownloads();
    return response.then((value) {
      final List<Product> result = [];
      final data = json.decode(value.body);
      data['downloads'].forEach((s) {
        final res = Product.fromJson(s);
        result.add(res);
      });
      return PackagesDataState(result);
    }, onError: (error) {
      throw error;
    });
  }
}

class PackagesDataState extends ItemDataState<List<Product>> {
  PackagesDataState(List<Product> data) : super(data);
}
