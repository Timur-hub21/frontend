import 'package:flutter/material.dart';

class HomePageContentPadding extends StatelessWidget {
  static const EdgeInsets value = EdgeInsets.symmetric(horizontal: 64.0, vertical: 32);
  final Widget child;
  final double? height;
  final EdgeInsets padding;

  const HomePageContentPadding(
      {required this.child, this.height = 610 - kToolbarHeight, this.padding = value});

  @override
  Widget build(BuildContext context) {
    return SizedBox(width: 1280, height: height, child: Padding(padding: padding, child: child));
  }
}
