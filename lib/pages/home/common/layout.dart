import 'package:flutter/widgets.dart';
import 'package:responsive_builder/responsive_builder.dart';

class HomeLayoutBuilder extends StatelessWidget {
  final Widget mobile;
  final Widget desktop;

  const HomeLayoutBuilder(this.mobile, this.desktop);

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizingInformation) {
      if (sizingInformation.isDesktop) {
        return desktop;
      }
      return mobile;
    });
  }
}
