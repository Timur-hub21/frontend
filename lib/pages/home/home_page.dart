import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:flutter_common/widgets.dart';
import 'package:frontend/brand.dart';
import 'package:frontend/common/animated_logo.dart';
import 'package:frontend/fetcher.dart';
import 'package:frontend/models/destination.dart';
import 'package:frontend/models/widgets/settings.dart';
import 'package:frontend/routing/route_config.dart';
import 'package:frontend/routing/router_delegate.dart';
import 'package:frontend/service_locator.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  const HomePage();

  static List<Destination> destinations() {
    return <Destination>[
      Destination(const RouteConfig.home(), 'home'.tr(), Icons.home),
      Destination(const RouteConfig.downloads(), 'downloads'.tr(), Icons.download_outlined)
    ];
  }

  @override
  _HomePageState createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
    final fetcher = locator<Fetcher>();
    final resp = fetcher.getInfo();
    resp.then((ServerInfo info) {
      if (info.providersCount == 0) {
        WidgetsBinding.instance.addPostFrameCallback((_) {
          showDialog(context: context, builder: (_) => const _NoProvidersDialog());
        });
      }
    }, onError: (error) {});
  }

  @override
  Widget build(BuildContext context) {
    final brand = Provider.of<BrandInfo>(context);
    return Center(child: AnimatedLogoButton(path: brand.brand.logo, size: 512));
  }
}

class _NoProvidersDialog extends StatelessWidget {
  const _NoProvidersDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Hello!'),
      content: const Text('There are no providers yet. Want to create one?', softWrap: true),
      actions: <Widget>[
        FlatButtonEx.notFilled(text: 'cancel'.tr(), onPressed: Navigator.of(context).pop),
        FlatButtonEx.filled(text: 'Create', onPressed: () => _create(context))
      ],
    );
  }

  void _create(BuildContext context) {
    Navigator.pop(context);
    RouterDelegateEx.of(context).setNewRoutePath(const RouteConfig.signUp());
  }
}
