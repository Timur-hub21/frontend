import 'package:flutter/material.dart';

class PrivacyPolicy extends StatelessWidget {
  final String project;
  const PrivacyPolicy({required this.project});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(children: [
      Expanded(
        child: PrivacyPolicyText(project),
      ),
    ]));
  }
}

class PrivacyPolicyText extends StatelessWidget {
  final String project;
  const PrivacyPolicyText(this.project, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: RichText(
          text: TextSpan(
        children: <TextSpan>[
          const TextSpan(
              text: 'We Respect and Protect Your Privacy\n\n',
              style: TextStyle(decoration: TextDecoration.underline)),
          const TextSpan(text: '- We only collect necessary information\n\n'),
          const TextSpan(
              text: '- We will only collect your usage information for analytic purposes\n\n'),
          const TextSpan(text: '- We protect your data\n\n'),
          const TextSpan(text: '- You have access to your data\n\n'),
          const TextSpan(
              text: 'We don’t Share Customer Information\n\n',
              style: TextStyle(decoration: TextDecoration.underline)),
          TextSpan(
              text:
                  '$project does not share customer information of any kind with anyone or any third party. We will not sell or rent your name or personal information to any third party. We do not sell, rent or provide outside access to our mailing list or any data we store. Any data that a user stores via our facilities is wholly owned by that user or business. At anytime a user or business is free to take their data and leave, or to simply delete their data from our facilities.\n\n'),
          const TextSpan(
              text: 'WE PROTECT YOUR INFORMATION\n\n',
              style: TextStyle(decoration: TextDecoration.underline)),
          const TextSpan(
              text:
                  'We implement a variety of security measures to maintain the safety of your personal information when you place an order. We offer the use of a secure server. After a transaction, your private information (credit cards, financials, etc.) will not be stored on our servers.\n\n'),
          const TextSpan(
              text: 'We Follow Laws\n\n', style: TextStyle(decoration: TextDecoration.underline)),
          TextSpan(
              text:
                  '$project may release personal information if the partnership is required to by law, search warrant, subpoena, court order or fraud investigation. We may also use personal information in a manner that does not identify you specifically nor allow you to be contacted but does identify certain criteria about our Site’s users in general (such as we may inform third parties about the number of registered users, number of unique visitors, number of $project instances linked, average number of keys by instance, and the pages most frequently browsed, etc.).\n\n'),
          const TextSpan(text: 'We Talk\n\n', style: TextStyle(decoration: TextDecoration.underline)),
          TextSpan(
              text:
                  'If a customer or potential customer of $project has any specific questions, concerns or complaints about our privacy policies or the methods in which we collect the necessary data to run our business, they are encouraged to contact us and one of our team members will respond promptly and will work with them to find an amicable resolution to the issue.\n\n'),
          const TextSpan(
              text: 'CONTACTING US\n\n', style: TextStyle(decoration: TextDecoration.underline)),
          const TextSpan(
              text:
                  'If there are any questions regarding this privacy policy you may contact us using the information available on the support page.'),
        ],
      )),
    );
  }
}
