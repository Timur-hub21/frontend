import 'package:flutter_common/flutter_common.dart';
import 'package:frontend/event_bus.dart';
import 'package:frontend/fetcher.dart';
import 'package:frontend/shared_prefs.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

Future setupLocator() async {
  final fetcher = Fetcher.getInstance();
  locator.registerSingleton<Fetcher>(fetcher);

  final eventBus = await FastoEventBus.getInstance();
  locator.registerSingleton<FastoEventBus>(eventBus);

  final package = await PackageManager.getInstance();
  locator.registerSingleton<PackageManager>(package);

  final storage = await LocalStorageService.getInstance();
  locator.registerSingleton<LocalStorageService>(storage);
}
