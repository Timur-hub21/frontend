import 'package:fastocloud_dart_models/models.dart';

class SubscriberAddEvent {
  final Subscriber sub;

  SubscriberAddEvent(this.sub);
}

class SubscriberEditEvent {
  final Subscriber sub;

  SubscriberEditEvent(this.sub);
}

class SubscriberRemoveEvent {
  final Subscriber sub;

  SubscriberRemoveEvent(this.sub);
}
