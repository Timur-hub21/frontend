import 'package:fastocloud_dart_models/models.dart';

class PackageAddEvent {
  final GoServer server;

  PackageAddEvent(this.server);
}

class PackageEditEvent {
  final GoServer server;

  PackageEditEvent(this.server);
}

class PackageRemoveEvent {
  final GoServer server;

  PackageRemoveEvent(this.server);
}
