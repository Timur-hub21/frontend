import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:frontend/service_locator.dart';
import 'package:frontend/shared_prefs.dart';

class ProfileInfo extends StatefulWidget {
  final Widget child;

  const ProfileInfo({required this.child});

  static _ProfileInfoState? of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<_InheritedProfile>()!.data;
  }

  @override
  _ProfileInfoState createState() {
    return _ProfileInfoState();
  }
}

class _ProfileInfoState extends State<ProfileInfo> {
  Provider? _profile;

  Provider? get profile => _profile;

  @override
  Widget build(BuildContext context) {
    return _InheritedProfile(data: this, child: widget.child);
  }

  void updateProfile(Provider newProfile) {
    if (_profile == null) {
      setState(() {
        _profile = newProfile;
      });
    }
  }

  void logout() {
    final settings = locator<LocalStorageService>();
    settings.setCheck(null);
    settings.setEmail(null);
    settings.setPassword(null);
    setState(() {
      _profile = null;
    });
  }
}

class _InheritedProfile extends InheritedWidget {
  final _ProfileInfoState? data;

  const _InheritedProfile({
    this.data,
    Key? key,
    required Widget child,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_InheritedProfile oldWidget) {
    return true;
  }
}
