const String TR_LOCALIZATION = "Localization";
const String TR_LANGUAGE = 'Language';
const String TR_LANGUAGE_CHOOSE = 'Choose Language';
const String TR_LANGUAGE_NAME = 'Language name';

const TR_SIGN_IN = 'Sign in';
const TR_SIGN_UP = 'Sign up';
const TR_DOWNLOADS = 'Downloads';
const TR_DEVELOPERS = 'Developers';
const TR_PRICING = 'Pricing';
const TR_BECOME_A_PARTNER = 'Become translations Partner';
const TR_FIND_A_PARTNER = 'Find translations Partner';

const TR_EMAIL = 'Email';
const TR_URL = 'Url';
const TR_EXTENSION = 'Extension';
const TR_CREATED_DATE = 'Created date';
const TR_ACTIONS = 'Actions';
const TR_CLOSE = 'Close';
const TR_EXP_DATE = 'Expiration date';
const TR_STATUS = 'Status';
const TR_TYPE = 'Type';
const TR_TITLE = 'Title';
const TR_CONTENT_REQUESTS = 'Content Requests';
const TR_DEVICES = 'Devices';
const TR_MAX_DEVICES_COUNT = 'Max device count';
const TR_HOME = 'Home';
const TR_PROFILE = 'Profile';
const TR_SUBSCRIBERS = 'Subscribers';
const TR_PROVIDERS = 'Providers';
const TR_PACKAGES = 'Packages';
const TR_SETTINGS = 'Settings';
const TR_SERIES = 'Series';
const TR_LOGOUT = 'Logout';
const TR_ADD = 'Add';
const TR_SYNC = 'Sync';
const TR_REMOVE = 'Remove';
const TR_ACTIVATE = 'Activate';
const TR_CANCEL = 'Cancel';
const TR_OK = 'OK';
const TR_SAVE = 'Save';
const TR_EDIT = 'Edit';
const TR_NAME = 'Name';

// servers stats
const TR_ID = 'Id';
const TR_LOAD_AVERAGE = 'Average load';
const TR_MEMORY = 'Memory';
const TR_HDD = 'HDD';
const TR_MEMORY_TOTAL = 'Total';
const TR_MEMORY_FREE = 'Free';
const TR_MEMORY_USED = 'Used';
const TR_NETWORK = 'Network';
const TR_VERSION = 'Version';
const TR_UPTIME = 'Uptime';
const TR_SYNCTIME = 'Synctime';
const TR_TIMESTAMP = 'Timestamp';
const TR_ONLINE_USERS = 'Online users';
const TR_OS = 'OS';
const TR_OS_ARCH = 'Arch';

const TR_FOLLOW_US = 'Follow us';
const TR_ABOUT = 'About';
const TR_EDUCATION_AND_SUPPORT = 'Education and support';

