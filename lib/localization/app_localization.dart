import 'package:flutter/material.dart';

class AppLocalization {
  static const Locale en = Locale('en');
  static const Locale ru = Locale('ru');

  static List<Locale> get supportedLocales => [en, ru];
}
