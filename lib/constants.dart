const String LOGO_PATH = 'install/assets/logo.png';
const String PROJECT = 'FastoCloud OTT';
const String NET_LOGO_URL = 'https://api.fastogt.com/static/projects/fastocloud_ott.png';
const String LANDING_URL = 'https://ott.fastotv.com';

const String CONTACT_US_EMAIL = 'support@fastogt.com';

const String FAQ_URL = 'https://github.com/fastogt/fastocloud_docs/wiki';
const String INSTALLATION_MEDIA_PART_URL =
    'https://github.com/fastogt/fastocloud_env/wiki/Install-package';
const String MEDIA_API_DOCS_URL =
    'https://fastogt.stoplight.io/docs/fastocloud-api/reference/fastocloud_streams.yaml';
const String LICENSE_GEN_URL = 'https://license.fastogt.com';
