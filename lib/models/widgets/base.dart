import 'package:easy_localization/easy_localization.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';
import 'package:frontend/common/streams/add_edit/sections/common.dart';

class HostAndPortTextField extends StatelessWidget {
  final HostAndPort value;

  final String hostHint;
  final String hostError;
  final String portHint;
  final String portError;
  final int ratio;

  const HostAndPortTextField(
      {required this.value,
      this.hostHint = 'Host',
      this.hostError = 'Input host',
      this.portHint = 'Port',
      this.portError = 'Input port',
      this.ratio = 2});

  @override
  Widget build(BuildContext context) {
    return Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[Expanded(flex: ratio, child: _host()), Expanded(child: _port())]);
  }

  Widget _host() {
    return TextFieldEx(
        validator: (String text) {
          return HostAndPort.isValidHost(text) ? null : hostError;
        },
        hintText: hostHint,
        errorText: hostError,
        init: value.host,
        onFieldChanged: (val) {
          value.host = val;
        });
  }

  Widget _port() {
    return NumberTextField.integer(
        canBeEmpty: false,
        hintText: 'Port',
        initInt: value.port,
        onFieldChangedInt: (val) {
          if (val != null) value.port = val;
        });
  }
}

class WSServerTextField extends StatelessWidget {
  final WSServer value;

  final String urlHint;
  final String urlError;

  const WSServerTextField({required this.value, this.urlHint = 'URL', this.urlError = 'Input URL'});

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Row(children: [Expanded(flex: 2, child: _url())]),
      Row(children: [Expanded(flex: 2, child: _useLoginAndPasswordField())])
    ]);
  }

  Widget _url() {
    return TextFieldEx(
        validator: (String text) {
          return WSServer(url: value.url).isValid() ? null : urlError;
        },
        hintText: urlHint,
        errorText: urlError,
        init: value.url,
        onFieldChanged: (val) {
          value.url = val;
        });
  }

  Widget _useLoginAndPasswordField() {
    return OptionalFieldTile(
        title: 'use-login-and-password'.tr(),
        init: value.needAuth(),
        onChanged: (state) {
          if (state) {
            value.password = WSServer.DEFAULT_PASSWORD;
            value.login = WSServer.DEFAULT_LOGIN;
          } else {
            value.login = null;
            value.password = null;
          }
        },
        builder: () {
          return Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [Expanded(child: _loginField()), Expanded(child: _passwordField())]);
        });
  }

  Widget _loginField() {
    return TextFieldEx(
      hintText: 'login'.tr(),
      errorText: '${'enter'.tr()} ${'login'.tr()}',
      init: value.login,
      keyboardType: TextInputType.emailAddress,
      onFieldChanged: (val) => value.login = val,
    );
  }

  Widget _passwordField() {
    return PassWordTextField(
      hintText: 'password'.tr(),
      errorText: '${'enter'.tr()} ${'password'.tr()}',
      init: value.password,
      onFieldChanged: (val) => value.password = val,
    );
  }
}
