import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class CommonActionOttIcon extends IconButton {
  final VoidCallback? onTap;

  CommonActionOttIcon.addProvider(this.onTap)
      : super(icon: const Icon(Icons.wifi_tethering), tooltip: 'add-provider'.tr(), onPressed: onTap);

  CommonActionOttIcon.removeProvider(this.onTap)
      : super(
            icon: const Icon(Icons.portable_wifi_off),
            tooltip: 'remove-provider'.tr(),
            onPressed: onTap);

  CommonActionOttIcon.details(this.onTap)
      : super(icon: const Icon(Icons.list), tooltip: 'details'.tr(), onPressed: onTap);

  CommonActionOttIcon.edit(this.onTap)
      : super(icon: const Icon(Icons.edit), tooltip: 'edit'.tr(), onPressed: onTap);

  CommonActionOttIcon.copy(this.onTap)
      : super(icon: const Icon(Icons.content_copy), tooltip: 'copy'.tr(), onPressed: onTap);

  CommonActionOttIcon.remove(this.onTap)
      : super(icon: const Icon(Icons.delete), tooltip: 'remove'.tr(), onPressed: onTap);
}
