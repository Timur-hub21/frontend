import 'package:easy_localization/easy_localization.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';
import 'package:frontend/common/streams/add_edit/sections/common.dart';

class SaveImagesTile extends StatefulWidget {
  final MachineLearning machine;

  const SaveImagesTile(this.machine);

  @override
  _SaveImagesTileState createState() => _SaveImagesTileState();
}

class _SaveImagesTileState extends State<SaveImagesTile> {
  bool get saveImages => widget.machine.dump;

  @override
  Widget build(BuildContext context) {
    return OptionalFieldTile(
        title: '${'save'.tr()} ${'images'.tr()}',
        init: saveImages,
        onChanged: (value) {
          setState(() {
            widget.machine.dump = value;
          });
        },
        builder: _classIDField);
  }

  Widget _classIDField() {
    const List<String> values = [
      'person',
      'bicycle',
      'car',
      'motorbike',
      'aeroplane',
      'bus',
      'train',
      'truck',
      'boat',
      'traffic light',
      'fire hydrant',
      'stop sign',
      'parking meter',
      'bench',
      'bird',
      'cat',
      'dog',
      'horse',
      'sheep',
      'cow',
      'elephant',
      'bear',
      'zebra',
      'giraffe',
      'backpack',
      'umbrella',
      'handbag',
      'tie',
      'suitcase',
      'frisbee',
      'skis',
      'snowboard',
      'sports ball',
      'kite',
      'baseball bat',
      'baseball glove',
      'skateboard',
      'surfboard',
      'tennis racket',
      'bottle',
      'wine glass',
      'cup',
      'fork',
      'knife',
      'spoon',
      'bowl',
      'banana',
      'apple',
      'sandwich',
      'orange',
      'broccoli',
      'carrot',
      'hot dog',
      'pizza',
      'donut',
      'cake',
      'chair',
      'sofa',
      'pottedplant',
      'bed',
      'diningtable',
      'toilet',
      'tvmonitor',
      'laptop',
      'mouse',
      'remote',
      'keyboard',
      'cell phone',
      'microwave',
      'oven',
      'toaster',
      'sink',
      'refrigerator',
      'book',
      'clock',
      'vase',
      'scissors',
      'teddy bear',
      'hair drier',
      'toothbrush'
    ];

    int findIndex(String? label) {
      for (int i = 0; i < values.length; ++i) {
        if (values[i] == label) {
          return i;
        }
      }
      return 0;
    }

    return DropdownButtonEx<String>(
        value: values[widget.machine.classId],
        values: values,
        onChanged: (c) {
          setState(() {
            widget.machine.classId = findIndex(c);
          });
        },
        itemBuilder: (String data) {
          return DropdownMenuItem(child: Text(data), value: data);
        });
  }
}
