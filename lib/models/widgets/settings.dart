import 'package:fastocloud_dart_models/models.dart';

class PrivateSettings extends OttSettings {
  static const PAYMENT_FIELD = 'payment';

  IPayment? payment;

  PrivateSettings(
      {required String logo,
      required String title,
      required String landing,
      required GlobalTheme theme,
      this.payment})
      : super(
          logo: logo,
          title: title,
          landing: landing,
          theme: theme,
        );

  factory PrivateSettings.fromJson(Map<String, dynamic> json) {
    final base = OttSettings.fromJson(json);
    IPayment? payment;
    if (json.containsKey(PAYMENT_FIELD)) {
      payment = IPayment.fromJson(json[PAYMENT_FIELD]);
    }
    return PrivateSettings(
        logo: base.logo,
        title: base.title,
        landing: base.landing,
        theme: base.theme,
        payment: payment);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    if (payment != null) {
      result[PAYMENT_FIELD] = payment!.toJson();
    }
    return result;
  }
}

class ServerInfo {
  static const _PROVIDERS_COUNT_FIELD = 'providers_count';
  static const _BRAND_FIELD = 'brand';

  int providersCount;
  OttSettings brand;

  ServerInfo({required this.providersCount, required this.brand});

  factory ServerInfo.fromJson(Map<String, dynamic> json) {
    return ServerInfo(
        providersCount: json[_PROVIDERS_COUNT_FIELD],
        brand: OttSettings.fromJson(json[_BRAND_FIELD]));
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = {};
    result[_PROVIDERS_COUNT_FIELD] = providersCount;
    result[_BRAND_FIELD] = brand.toJson();
    return result;
  }
}
