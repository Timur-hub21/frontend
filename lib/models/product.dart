import 'package:fastocloud_dart_models/models.dart';

class Pack {
  static const VERSION_FIELD = 'version';
  static const URL_FIELD = 'url';

  String version;
  String url;

  Pack({required this.version, required this.url});

  factory Pack.fromJson(Map<String, dynamic> json) {
    final url = json[URL_FIELD];
    final version = json[VERSION_FIELD];
    return Pack(url: url, version: version);
  }

  Map<String, dynamic> toJson() {
    return {URL_FIELD: url, VERSION_FIELD: version};
  }
}

class Package extends OperationSystem {
  static const PACKAGE_FIELD = 'package';

  Pack package;

  Package(
      {required String name, required String version, required String arch, required this.package})
      : super(name: name, version: version, arch: arch);

  factory Package.fromJson(Map<String, dynamic> json) {
    final base = OperationSystem.fromJson(json);
    final package = Pack.fromJson(json[PACKAGE_FIELD]);
    return Package(name: base.name, version: base.version, arch: base.arch, package: package);
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> result = super.toJson();
    result[PACKAGE_FIELD] = package.toJson();
    return result;
  }
}

class Product {
  static const NAME_FIELD = 'name';
  static const DESCRIPTION_FIELD = 'description';
  static const PACKAGES_FIELD = 'packages';

  String name;
  String description;
  List<Package> packages;

  Product({required this.name, required this.description, required this.packages});

  factory Product.fromJson(Map<String, dynamic> json) {
    final String name = json[NAME_FIELD];
    final String description = json[DESCRIPTION_FIELD];
    final List<Package> packages = [];
    json[PACKAGES_FIELD].forEach((element) => packages.add(Package.fromJson(element)));
    return Product(name: name, description: description, packages: packages);
  }

  Map<String, dynamic> toJson() {
    return {NAME_FIELD: name, DESCRIPTION_FIELD: description, PACKAGES_FIELD: packages};
  }
}
