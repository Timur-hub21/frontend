import 'package:flutter/widgets.dart';
import 'package:frontend/routing/route_config.dart';

class Destination {
  final RouteConfig route;
  final String title;
  final IconData icon;

  const Destination(this.route, this.title, this.icon);
}
